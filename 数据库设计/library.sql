/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : library

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2018-11-22 10:15:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `book`
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `bookid` int(10) NOT NULL AUTO_INCREMENT,
  `typeid` int(10) NOT NULL,
  `bookname` varchar(100) NOT NULL,
  `locationid` int(10) NOT NULL,
  `booknum` int(10) NOT NULL COMMENT '书籍总量',
  `booksurplus` int(10) NOT NULL COMMENT '书籍剩余量',
  `summary` varchar(100) NOT NULL COMMENT '书籍简介',
  `clicknum` int(10) NOT NULL COMMENT '点击量',
  `bookstate` int(10) NOT NULL COMMENT '书籍的禁用启用',
  `resource` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`bookid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------

-- ----------------------------
-- Table structure for `booktype`
-- ----------------------------
DROP TABLE IF EXISTS `booktype`;
CREATE TABLE `booktype` (
  `typeid` int(10) NOT NULL AUTO_INCREMENT,
  `typename` varchar(100) NOT NULL,
  `typerent` double(10,2) NOT NULL COMMENT '书类',
  `breakrent` double(10,2) NOT NULL COMMENT '违约后的租金',
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of booktype
-- ----------------------------

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `commentid` int(10) NOT NULL AUTO_INCREMENT,
  `bookid` int(10) NOT NULL,
  `commenttitle` varchar(100) NOT NULL,
  `content` varchar(100) NOT NULL,
  PRIMARY KEY (`commentid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for `location`
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `locationid` int(10) NOT NULL,
  `locationname` varchar(100) NOT NULL,
  PRIMARY KEY (`locationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of location
-- ----------------------------
INSERT INTO `location` VALUES ('11', '101室');
INSERT INTO `location` VALUES ('12', '102室');
INSERT INTO `location` VALUES ('13', '103室');
INSERT INTO `location` VALUES ('14', '104室');
INSERT INTO `location` VALUES ('15', '105室');
INSERT INTO `location` VALUES ('21', '201室');
INSERT INTO `location` VALUES ('22', '202室');
INSERT INTO `location` VALUES ('23', '203室');
INSERT INTO `location` VALUES ('24', '204室');
INSERT INTO `location` VALUES ('25', '205室');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `wallet` double(10,2) NOT NULL COMMENT '账户余额',
  `phone` varchar(100) NOT NULL,
  `idcard` varchar(100) NOT NULL,
  `userstate` int(10) NOT NULL COMMENT '用户的禁用启用',
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for `userborrow`
-- ----------------------------
DROP TABLE IF EXISTS `userborrow`;
CREATE TABLE `userborrow` (
  `borrowid` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `typeid` int(10) NOT NULL,
  `bookid` int(10) NOT NULL,
  `borrowtime` datetime NOT NULL,
  `daylong` int(10) NOT NULL,
  `state` int(10) NOT NULL,
  PRIMARY KEY (`borrowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userborrow
-- ----------------------------
