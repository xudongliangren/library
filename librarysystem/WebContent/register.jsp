<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js" lang="zxx">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>注册</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.jpg">
    
    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Style customizer (Remove these two lines please) -->
    <link rel="stylesheet" href="css/style-customizer.css">
    <link href="#" data-style="styles" rel="stylesheet">
    
    <!-- Modernizr JS -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  
    <!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Start of header area -->
        
		<section class="breadcrumbs-area bg-3 ptb-110 bg-opacity bg-relative">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="">
                           
                            <ul>
                                <li>
                                    <a class="active" href="#">注册页面</a>
                                </li>
                              
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      
        <!-- End page content -->
        <!-- Start footer area -->
        <footer class="footer-area">
            <div class="footer-top ptb-110 navy-bg">
                <div class="container">
                    <div class="row">
                     
                        <div class="col-md-6 col-sm-6">
                            <div class="footer-text mrg-sm3 mrg-xs">
                               <h3>Register</h3>
                               <form action="#">
                                    <input placeholder="name*" type="text" id="username" name="username">
                                    <input  placeholder="pwd*" type="password" id="pwd" name="pwd">                                                      
                                    <input placeholder="phone" type="text" id="phone" name="phone"> 
                                    <input placeholder="idcard*" type="text" id="idcard" name="idcard">                          
                                    <textarea placeholder="remark*" type="text" id="remark" name="remark"></textarea>
                                   
                                    <input type="button" class="submit"  onclick="check();" value="注册">
                                    
                                  
                                </form>
                                
                            </div>
                      		<div >  <hr>
                       			<button class="submit"><a href="${pageContext.request.contextPath}/login.jsp" class="fl">立即登陆</a></button></div> 
        
                        </div>
                      
				</div>
			</div>
                    </div>
                </div>
            </div>
            
        </footer>
       
        <!--style-customizer end -->
    </div>
    <!-- Body main wrapper end -->
    
    
    
    
    <!-- Placed js at the end of the document so the pages load faster -->
    <!-- jquery latest version -->
    <script src="js/vendor/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- ajax-mail JS
    ============================================ -->		
    <script src="js/ajax-mail.js"></script>
    <!-- All js plugins included in this file. -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>
<script type="text/javascript">
	function check() {
		var username = $("#username").val();
		var pwd = $("#pwd").val();
		var phone = $("#phone").val();
		var idcard = $("#idcard").val();
		var remark = $("#remark").val();
		
		$.ajax({
					url : "${pageContext.request.contextPath}/register/registerUser",
					data : {
						username : username,
						pwd : pwd,
						phone : phone,
						idcard : idcard,
						remark : remark,
					},
					dataType : "json",
					success : function(data) {
						if (data.msg == 1) {
							alert("注册成功!")
							window.location.href = "${pageContext.request.contextPath}/login.jsp";
						} else {
							alert("注册失败，用户名已存在")
							window.location.reload();
						}
					}
				})
	}
</script>
</html>
