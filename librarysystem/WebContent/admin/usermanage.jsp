<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/framework.tld" prefix="mayi"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/base/base/css/H-ui.css" />
<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/base/base/css/H-ui.admin.css" />
<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/base/base/font/font-awesome.min.css" />

<title></title>
</head>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	//带条件的分页查询
	function jump(curPage) {
		$("#curPage").val(curPage);//修改隐藏域的页码
		//还可以进行 form的 提交的url
		$("#form1").action = "${pageContext.request.contextPath }/admin/selectByCourseName";
		//提交form表单
		$("#form1").submit();
	}
</script>
<body>
	<nav class="Hui-breadcrumb"> <i class="icon-home"></i> 管理员模块 <span
		class="c-gray en">&gt;</span> 用户管理</nav>
	<div class="pd-20">
		<div class="text-c">
			<form
				action="${pageContext.request.contextPath }/admin/selectUsersByName"
				method="post" id="form1">
			 <input type="text" class="input-text" style="width: 250px"
					id="" name="username" value="${username}">
					<input type="submit" class="btn btn-success" onclick="javascript:jump(1)" value="搜索">
								<!-- 隐藏域 -->
				<input type="hidden" id="curPage" name="curPage" value="">
			</form>
		</div>
		<div class="cl pd-5 bg-1 bk-gray mt-20">
						<span class="l"> <!--说明：user_add 在  \base\base\js\H-ui.admin.js 中定义-->
			<%-- 	<a href="${pageContext.request.contextPath}/admin/toAddNewBook"
				
				class="btn btn-primary radius"><i class="icon-plus"></i> 新增书籍</a> --%></span> 
			<span class="r">共有数据：<strong>${page.date.size()}</strong>
				条
			</span>
			<%-- <a href="${pageContext.request.contextPath}/teacher/loginOut.do"><button type="submit" class="btn btn-success"> 注销</a> --%>
			</button>
			</span>
		</div>

		<table class="table table-border table-bordered table-bg">
			<thead>
				<tr>
					<th scope="col" colspan="11">用户列表</th>
				</tr>
				<tr class="text-c">
					<th width="70">用户名</th>
					<th width="70">密码</th>
					<th width="130">账户余额</th>
					<th width="130">电话</th>
					<th width="30">身份证号</th>
					<th width="40">备注</th>
					<th width="40">状态</th>
					<th style="width: 100px;">操作</th>
				</tr>
			</thead>
			<c:forEach items="${page.date}" var="list">
				<tbody>

					<tr class="text-c">
						<td>${list.username}</td>
						<td>${list.pwd}</td>
						<td>${list.wallet}</td>
						<td>${list.phone}</td>
						<td>${list.idcard}</td>
						<td>${list.remark}</td>
						<td><c:if test="${list.userstate==1}">
								<span>启用</span>
							</c:if> <c:if test="${list.userstate==0}">
								<span>禁用</span>
							</c:if></td>
						<td class="f-14 admin-manage">
							<!--说明：user_show、curriculum_edit、curriculum_del在  \base\base\js\H-ui.admin.js 中定义-->
							<a title="查看借书历史"
							href="${pageContext.request.contextPath}/admin/showUserLog?userid=${list.userid}"
							class="ml-5" style="text-decoration: none"><i
							class="icon-edit"></i></a>
							 <a title="状态变更"
							href="${pageContext.request.contextPath}/admin/changeUserstate?userid=${list.userid}&&userstate=${list.userstate}"
							class="ml-5" style="text-decoration: none"><i
								class="icon-trash"></i></a>
							<a style="text-decoration: none"
							href="${pageContext.request.contextPath }/admin/checkUserComment?userid=${list.userid }" 
							title="查看他的评论" ><i
							class="icon-eye-open"></i></a>
						</td>
					</tr>

				</tbody>
			</c:forEach>
		</table>
		<!-- </div>
 -->
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/js/jquery.min.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/layer/layer.min.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/js/pagenav.cn.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/js/H-ui.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/plugin/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/base/base/js/H-ui.admin.js"></script>
		<script type="text/javascript">
			/* 		function detail(){
			 window.location.href=""
			 }
			 function del(){

			 window.location.href = "${pageContext.request.contextPath}/teacher/deleteCourse.do?curriculumid=3"
			 }
			 function update(){
			 window.location.href = "${pageContext.request.contextPath}/teacher/toUpdate.do?curriculumid=2"
			 }
			 function add(){
			 window.location.href = "${pageContext.request.contextPath}/teacher/toAddCourse.do"
			 } */
		</script>
		<div align="center">
			<p>
				<c:if test="${page.curPage == 1}">首页</c:if>
				<c:if test="${page.curPage > 1}">
					<a href="javascript:jump(1)">首页</a>
				</c:if>
				<c:if test="${page.curPage == 1 }">上一页</c:if>
				<c:if test="${page.curPage > 1 }">
					<a href="javascript:jump(${page.curPage-1 })">上一页</a>
				</c:if>
				<c:if test="${page.curPage == page.totalPage }">下一页</c:if>
				<c:if test="${page.curPage < page.totalPage }">
					<a href="javascript:jump(${page.curPage+1 })">下一页</a>
				</c:if>
				<c:if test="${page.curPage == page.totalPage }">末页</c:if>
				<c:if test="${page.curPage < page.totalPage }">
					<a href="javascript:jump(${page.totalPage })">末页</a>
				</c:if>
				共${page.totalPage }页 当前页${page.curPage } 总共${page.rows }条数据
			</p>
		</div>
		<br />
</body>
</html>