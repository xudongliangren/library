<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script type="text/javascript">
	function isNotNull(uuid) {
		var value = document.getElementById(uuid).value;
		if (value == null || value == "") {
			return false;
		} else {
			return true;
		}
	}
	function check() {
		if (isNotNull('bookname') && isNotNull('booknum') && isNotNull('resource') && isNotNull('summary')) {
			return true;
		} else {
			alert("添加失败，请将信息填写完整！");
			return false;
		}
	}
</script>
<body>
	<div class="pd-20">
		<div class="Huiform">
			<form action="${pageContext.request.contextPath }/admin/addNewBook"
				method="post" target="_parent" onsubmit="return check();">
				<table class="table table-bg">
					<tbody>
						<tr>
							<th width="100" class="text-r">书名：</th>
							<td><input type="text" style="width: 200px"
								class="input-text" value="" placeholder="" id="bookname"
								name="bookname" datatype="*1-10"></td>
						</tr>
						<tr>
							<th width="100" class="text-r">书籍类型：</th>

							<td><select style="width: 210px; height: 29px;"
								class="input-text" id="typeid" name="typeid">
									<option value="">- - - 请选择书类 - - -</option>
									<c:forEach items="${btlist}" var="btlist">
										<option value="${btlist.typeid}">${btlist.typename}</option>
									</c:forEach>
							</select></td>

						</tr>
						<tr>
							<th width="100" class="text-r">书籍位置：</th>

							<td><select style="width: 210px; height: 29px;"
								class="input-text" id="locationid" name="locationid">
									<option value="">- - - 请选择书籍位置 - - -</option>
									<c:forEach items="${locationlist}" var="locationlist">
										<option value="${locationlist.locationid}">${locationlist.locationname}</option>
									</c:forEach>
							</select></td>
						</tr>
						<tr>
							<th width="100" class="text-r">总本数：</th>
							<td><input type="text" style="width: 200px"
								name="booknum" class="input-text" datatype="*1-18" id="booknum"></td>
						</tr>
						<tr>
							<th width="100" class="text-r">图片资源位置：</th>
							<td><input type="text" style="width: 200px"
								name="resource" class="input-text" datatype="*1-18"
								id="resource"></td>
						</tr>
						<tr>
							<th class="text-r">简介：</th>
							<td><textarea class="input-text" name="summary" id="summary"
									style="height: 100px; width: 300px;"></textarea></td>
						</tr>
						<tr>
							<th></th>
							<td><input type="submit" name="button" class="icon-ok"
								id="btnOk" value="添加" /></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
	</div>

	<script type="text/javascript"
		src="<%=request.getContextPath()%>/base/base/js/jquery.min.1.8.1.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>../../base/base/js/H-ui.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/base/base/js/H-ui.admin.js"></script>
	<script type="text/javascript">
		$(".Huiform").Validform();
		$(function() {

		});
	</script>
</body>
</html>