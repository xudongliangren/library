<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员 - 中心</title>
<link rel="stylesheet"
	href="<%=request.getContextPath() %>/base/admin/css/pt_home_1492073290244.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/1.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/2.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/3.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/4.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/5.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/6.css">
<script src="<%=request.getContextPath() %>/base/admin/js/libRegular_1492073288984.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/core_1492073290293.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/libEs5Shim_1492073288982.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/pt_home_1492073290295.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/webzj_cdn101_pathb_message_17011101.js"></script>
<script src="<%=request.getContextPath() %>/base/user/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
        function onSelectedTag(obj) {
            $("#div1").css("border-bottom","");
            $("#div2").css("border-bottom","");
            $("#div3").css("border-bottom","");
            $("#"+obj).css("border-bottom","4px solid #55B929");
        }
    </script>

</head>
<body>
	<div class="m-nav-container">
		<div class="m-navTop-func">
			<div class="navTop-func-f" style="background-color: #2e323e;">
				<div class="m-navTop-func_div47" title="">
					<div class="m-navTop-func_div48" title="">
						<a href="#"
							style="color: white; font-family: arial, 'Microsoft Yahei', 'Hiragino Sans GB', '微软雅黑', '宋体', \5b8b\4f53, Tahoma, Arial, Helvetica, STHeiti; font-size: 24px;">图书管理系统</a>
					</div>

					<div class="m-navTop-func_div81" title="">
						<div class="nav-searchFunc-i">
							<div class="m-navlinks">
								<a class="f-f0 navLoginBtn" id="auto-id-1492846128320"><span
									class="huo">欢迎管理员：</span>[<span style="color: red;">${loginUser.adminname}</span>]</a>
							</div>
						</div>
					</div>
					<div class="m-navTop-func_div86" title="">
						<span class="m-navTop-func_span87"><a
							class="navLoginBtn m-navTop-func_a89" id="auto-id-1492846128319"><span>我的课程<span></span></span></a></span>
					</div>
					<div class="m-navTop-func_div90" title="">
						<div class="nav-loginBox-i">
							<div class="m-navlinks" id="j-topnav">
								<div class="unlogin" style="width: 300px; margin-left: 40px;">
									<a class="f-f0 navLoginBtn" id="auto-id-1492846128320"
										href="<%=request.getContextPath() %>/login.jsp"><span class="huo"></span>注销</a>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>


		<div id="g-body">
			<div id="j-self-content" class=" top-box f-f0">
				<img class="top-bac" id="j-top-back" width="100%" height="50"
					src="../../base/admin/images/BCA790F6E742B8CBF078CA413D2ED7AD.png?imageView&amp;thumbnail=1920y220&amp;quality=100">
			</div>
			<div id="j-home-content" class="home-content">
				<div class="main-content f-cb">
					<div class="main-box f-fl">

						<div id="j-module-tab" class="module-tab">
							<div class="u-selectTab-container">
								<div class="u-st-course" id="div1"
									style="border-bottom: 4px solid #55B929;">
									<a href="${pageContext.request.contextPath }/admin/showAllBooks" target="admin_detail"
										onclick="onSelectedTag('div1');"> <span
										class="u-st-course_span2">书籍 </span>
									</a>
								</div>
								<div class="u-st-discuss" id="div2">
									<a href="${pageContext.request.contextPath }/admin/showAllUsers" target="admin_detail"
										onclick="onSelectedTag('div2');"> <span
										class="u-st-discuss_span4">用户 </span>
									</a>
								</div>
								<div class="u-st-home  f-dn">
									<a href="/home.htm?userId=" target="_top" class="u-st-home_a7">
										<span class="u-st-home_span8">个人中心&nbsp; <span
											class="u-icon-moreArrow"></span>
									</span>
									</a>
								</div>
								<div class="u-st-teachPage  f-dn">
									<a href="/u/?userId=" target="_top" class="u-st-teachPage_a9">
										<span class="u-st-teachPage_span10">进入老师主页&nbsp; <span
											class="u-icon-moreArrow"></span>
									</span>
									</a>
								</div>
							</div>
						</div>
						<div>
							<iframe src="${pageContext.request.contextPath }/admin/showAllBooks" name="admin_detail"
								style="width: 1024px; height: 500px;"></iframe>
						</div>
					</div>
					<div id="j-sidebar" class="sidebar-box f-fl"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>