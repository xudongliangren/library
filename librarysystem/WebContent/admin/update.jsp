<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>修改</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/frame/layui/css/layui.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/frame/static/css/style.css">
<link rel="icon"
	href="<%=request.getContextPath()%>/frame/static/image/code.png">
</head>

<body class="body">

	<form class="layui-form layui-form-pane"
		action="<%=request.getContextPath()%>/admin/updateBook">
		<!-- <div class="layui-form-item">
        <label class="layui-form-label">长输入框</label>

        <div class="layui-input-block">
            <input type="text" name="title" autocomplete="off" placeholder="请输入标题" lay-verify="required"
                   class="layui-input">
        </div>
    </div> -->
		<div class="layui-form-item">
			<label class="layui-form-label">书名</label>

			<div class="layui-input-inline">
				<input type="text" name="bookname" lay-verify="required"
					autocomplete="off" value="${bookinfo.bookname}" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">类名</label>

			<div class="layui-input-inline">
				<input type="text" name="typename" lay-verify="required"
					autocomplete="off" class="layui-input" value="${bookinfo.typename}">
			</div>

		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">存放位置</label>

			<div class="">
				<select name="locationname" class="" >
					<option value="">- - - 请选择书籍位置 - - -</option>
					<c:forEach items="${locationlist}" var="locationlist">
						<option value="${locationlist.locationid}">${locationlist.locationname}</option>
					</c:forEach>
				</select> 
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">总数</label>

			<div class="layui-input-inline">
				<input type="text" name="booknum" lay-verify="required"
					autocomplete="off" class="layui-input" value="${bookinfo.booknum}">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">余量</label>

			<div class="layui-input-inline">
				<input type="text" name="booksurplus" lay-verify="required"
					autocomplete="off" class="layui-input"
					value="${bookinfo.booksurplus}">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">点击量</label>

			<div class="layui-input-inline">
				<input type="text" name="clicknum" lay-verify="required"
					autocomplete="off" class="layui-input" value="${bookinfo.clicknum}">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">资源位置</label>

			<div class="layui-input-inline">
				<input type="text" name="resource" lay-verify="required"
					autocomplete="off" class="layui-input" value="${bookinfo.resource}">
			</div>
		</div>

		<div class="layui-form-item layui-form-text">
			<div class="layui-input-block">
				<textarea id="LAY_demo_editor" class="layui-textarea" name="summary">${bookinfo.summary}</textarea>
			</div>
			<label class="layui-form-label">简介</label>
		</div>
		<div class="layui-form-item">
			<button class="layui-btn" lay-submit="" lay-filter="sub">修改</button>
		</div>
	</form>
	<%-- <img src="${bookinfo.resource}"> --%>
	<script src="../frame/layui/layui.js" charset="utf-8"></script>

</body>
</html>