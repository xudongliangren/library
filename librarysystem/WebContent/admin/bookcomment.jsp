<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>课程- 评论中心</title>
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/user/css/comment2.css">

</head>
<body>
	
			<div class="course-info-main clearfix w">
				<div class="content-wrap clearfix" style="width: 100%;">
					<div class="content">
						<div id="course_note">
							
								<ul class="mod-post" id="comment-list">
								<c:forEach items="${commentlist}" var="commentVo">
									<li class="post-row" >
										
											<div class="media" >
												<a href="#" target="_blank"><img
													src="<%=request.getContextPath() %>/base/user/images/userimg.jpg" width="40" height="40"></a>
											</div>
											<div class="bd">
											<input type="checkbox" value="${commentVo.commentid }" name="commentid">
												<div class="footer clearfix">
													<span title="" class="l timeago">用户${commentVo.username}</span>
													<div class="actions r"></div>
													<p class="cnt">:${commentVo.content}</p>
												</div>		
											</div>
									</li>
									</c:forEach>
									<button onclick="del();">删除</button>
								</ul>
								
						</div>
					</div>
				</div>
			</div>
	
</body>
<script type="text/javascript">
	function del() {
		//var obj = $("input[name='userId']:checked");//获取所有checkbox被选中的对象
		var arr = [];
		$("input[name='userId']:checked").each(function(index, item) {//
			arr.push($(this).val());
		});
		alert(arr);
		window.location.href = "${pageContext.request.contextPath}/user/delete.do?ids="+ arr;// int[] ids
	}
</script>
</html>