<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>登录</title>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/frame/layui/css/layui.css">
    <link rel="stylesheet" href="<%=request.getContextPath() %>/frame/static/css/style.css">
    <link rel="icon" href="<%=request.getContextPath() %>/frame/static/image/code.png">
</head>
<body>
<div class="login-main">
    <header class="layui-elip">后台登录</header>
    <form class="layui-form" method="post" >
        <div class="layui-input-inline">
            <input type="text" name="username"  placeholder="姓名" autocomplete="off"
                   class="layui-input" id="username">
        </div>
        <div class="layui-input-inline">
            <input type="password" name="pwd" placeholder="密码" autocomplete="off"
                   class="layui-input" id="pwd">
        </div>
        <div class="layui-input-inline login-btn">
        <input type="button" class="layui-btn" onclick="checkInfo();" value="登陆">
        </div>
        <hr/>
        <p><a href="${pageContext.request.contextPath}/register.jsp" class="fl">立即注册</a></p>
    </form>
</div>


<script src="<%=request.getContextPath() %>/frame/layui/layui.js"></script>
<script type="text/javascript">
    
    function checkInfo(){
		var username = $("#username").val();
		var pwd = $("#pwd").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/login/sbLogin",
			data : {
				username : username,
				pwd : pwd,
			},
			dataType : "json",
			success : function(data){
				if(data.msg==2){
					alert("欢迎你,管理员！")
					window.location.href = "${pageContext.request.contextPath}/login/loginAdmin";
				}else if(data.msg==1){
					alert("欢迎你,亲爱的用户！")
					window.location.href = "${pageContext.request.contextPath}/login/loginUser";
				}else{
					alert("账号或密码错误！")
					window.location.reload();
				}
				}
	})
	}
</script>
</body>
</html>