<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/framework.tld" prefix="mayi"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>个人信息</title>
<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath() %>/base/base/css/H-ui.css" />
<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath() %>/base/base/css/H-ui.admin.css" />
<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath() %>/base/base/font/font-awesome.min.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	function pay(borrowid){
		
		$.ajax({
			url : "${pageContext.request.contextPath}/user/returnBook?borrowid="+borrowid,
			date : {
				borrowid : borrowid,
			},
			dataType : "json",
			success : function(data){
				if(data.msg==1){
					alert("扣费成功，本次消费"+data.money+"元！");
					window.location.href = "${pageContext.request.contextPath}/user/selectUserVoById";
				}else{
					alert("扣费失败，请重试或去柜台还书！");
					window.location.href = "${pageContext.request.contextPath}/user/selectUserVoById";
				}
			}
		})
	}
</script>
</head>
<body>
	<nav class="Hui-breadcrumb">
	<i class="icon-home"></i> 个人信息 <span class="c-gray en">&gt;</span>个人信息管理
	<a class="btn btn-success radius r mr-20"
		style="line-height: 1.6em; margin-top: 3px"
		href="javascript:location.replace(location.href);" title="刷新"><i
		class="icon-refresh"></i></a></nav>
	<button onclick="location.href='<%=request.getContextPath() %>/user/selcetAllBook'" class="btn btn-success radius" type="button">
			<i class="icon-remove"></i> 回主页
	</button>
	<div class="pd-20">
	
		<table
			class="table table-border table-bordered table-hover table-bg table-sort">
			<thead>
				<tr class="text-c">
					<th width="50">姓名</th>
					<th width="80">密码</th>
					<th width="70">钱包</th>
					<th width="150">手机号</th>
					<th width="50">身份证号</th>
					<th style="width: 80px;">借书记录</th>
					<th style="width: 100px;">操作</th>
				</tr>
			</thead>
			<tbody>
				
					<tr class="text-c">
						<td>${user.username}</td>
						<td>${user.pwd}</td>
						<td>${user.wallet}</td>
						<td>${user.phone}</td>
						<td>${user.idcard}</td>
						<td>${borrowVoList.size()}</td>
						<td>
							<button onclick="location.href='<%=request.getContextPath() %>/user/toUpdateUser'" class="btn btn-success radius" type="button">
									<i class="icon-remove"></i> 修改
							</button>
						</td>
						<td>
							<button onclick="location.href='<%=request.getContextPath() %>/user/toDeposit'" class="btn btn-success radius" type="button">
									<i class="icon-remove"></i> 充值
							</button>
						</td>
					</tr>
			</tbody>
		</table>
		借书记录
		<table
			class="table table-border table-bordered table-hover table-bg table-sort">
			<thead>
				<tr class="text-c">
					<th width="50">编号</th>
					<th width="80">书籍</th>
					<th width="70">借书时间</th>
					<th width="150">借书时长（天）</th>
					<th width="50">还书时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${borrowVoList}" var="borrowVo" varStatus="status">
					<tr class="text-c">
						<td>${status.index+1}</td>
						<td>${borrowVo.bookname}</td>
						<td>${borrowVo.borrowtime}</td>
						<td>${borrowVo.daylong}</td>
						<td>
							<c:if test="${borrowVo.state==1}">${borrowVo.returntime}</c:if>
							<c:if test="${borrowVo.state==0}">未还书</c:if>
						</td>
						<td>
								
							<c:if test="${borrowVo.state==0}">
							<button onclick="pay(${borrowVo.borrowid});" class="btn btn-success radius" type="button">
									<i class="icon-remove"></i> 还书
							</button>
							</c:if>
						</td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
	<script type="text/javascript" src="<%=request.getContextPath() %>/base/base/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/base/base/layer/layer.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/base/base/js/pagenav.cn.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/base/base/js/H-ui.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath() %>/base/base/plugin/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath() %>/base/base/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/base/base/js/H-ui.admin.js"></script>
	<script type="text/javascript">
		
	</script>
	
</body>
</html>

