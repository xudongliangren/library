<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>书籍详情</title>
<link rel="stylesheet"
	href="<%=request.getContextPath() %>/base/admin/css/pt_home_1492073290244.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/1.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/2.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/3.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/4.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/5.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/admin/css/6.css">
<script src="<%=request.getContextPath() %>/base/admin/js/libRegular_1492073288984.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/core_1492073290293.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/libEs5Shim_1492073288982.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/pt_home_1492073290295.js"></script>
<script src="<%=request.getContextPath() %>/base/admin/js/webzj_cdn101_pathb_message_17011101.js"></script>
<script src="<%=request.getContextPath() %>/base/user/js/jquery-1.10.2.min.js"></script>
<script src="<%=request.getContextPath() %>/base/user/js/comment.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/base/user/css/home-less.css">

</head>
<body>
<div class="m-nav-container">
    <div class="m-navTop-func">
        <div class="navTop-func-f" style="background-color: #2e323e;">
            <div class="m-navTop-func_div47" title="">
                <div class="m-navTop-func_div48" title="">
                    <a href="#" style="color: white;font-family: arial, 'Microsoft Yahei', 'Hiragino Sans GB', '微软雅黑', '宋体', \5b8b\4f53, Tahoma, Arial, Helvetica, STHeiti;
font-size: 24px;">栋梁图书馆平台</a>
                </div>
                <div class="m-navTop-func_div86" title=""><span class="m-navTop-func_span87"><a
                        class="navLoginBtn m-navTop-func_a89" id="auto-id-1492846128319"><span><span></span></span></a></span>
                </div>
                <div class="m-navTop-func_div90" title="">
                    <div class="nav-loginBox-i">
                        <div class="m-navlinks" id="j-topnav">
                            <div class="unlogin" style="width: 300px;margin-left: 40px;">
                            	<c:if test="${sessionScope.user!=null}">
                               		<a class="f-f0 navLoginBtn" href="${pageContext.request.contextPath}/login/loginOut2"><span class="huo"></span>注销</a>
                            	</c:if>
                            	<c:if test="${sessionScope.user==null}">
	                                     	<a href="${pageContext.request.contextPath}/user/selectUserVoById">登录</a>
	                                     	|<a href="${pageContext.request.contextPath}/register.jsp">注册</a>
	                            </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bindHintBox js-bindHintBox hide">
    <div class="pr">
        为了账号安全，请及时绑定邮箱和手机<a href="/user/setbindsns" class="ml20 color-red" target="_blank">立即绑定</a>
        <button  class="closeBindHint js-closeBindHint" type="button"></button>
        <div class="arrow"> </div>
    </div>
</div>
<div id="main">
    <div class="course-infos">
        <div class="w pr">
            <div class="path">
                <a href="../../CurriculumServlet?tag=show_allcurr_for_user">书籍</a>
                <i class="path-split">\</i><a href="#">${bookVo.typename}</a>
            </div>
            <div class="hd clearfix">
                <h2 class="l">${bookVo.bookname}</h2>
            </div>
            <div class="statics clearfix">
              <!--   <div class="moco-btn l learn-btn green-btn red-btn">
                    <a href="/code/48" class="J-learn-course">开始学习</a>
                    <em></em>
                    <i class="follow-action js-follow-action"></i>
                </div> -->
                <div class="static-item l">
                    <span class="meta">点击人数</span>
                    <span class="meta-value js-learn-num">${bookVo.clicknum}</span>
                    <em></em>
                </div>
            </div>
        </div>
        <div class="info-bg" id="js-info-bg">
            <div class="cover-img-wrap">
                <img data-src="http://img.mukewang.com/574678be0001b52806000338.jpg" alt="" style="display: none" id="js-cover-img">
            </div>
            <div class="cover-mask"></div>
        </div>
    </div>
    <div class="course-info-main clearfix w">
        <div class="content-wrap">
            <div class="content">
            <div id="notice" class="clearfix">
                        <strong>简介: </strong>
                        <a >${bookVo.summary}</a>
                </div>
                <div class="mod-tab-menu ">
                
                    <ul class="course-menu clearfix">
                        <li><a id="commentOn" class="" href="${pageContext.request.contextPath}/user/selectAllComment?bookid=${bookVo.bookid}" target="classVideo"><span>评论</span></a></li>
                   		 <li><a id="borrowbook" name = "borrowbook" 
	                   		 <c:if test="${sessionScope.user!=null}">
	                   		 	href="${pageContext.request.contextPath}/user/toBorrowBook?bookid=${bookVo.bookid}" 
	                   		 </c:if>
                   		 	<c:if test="${sessionScope.user==null}">
                   		 		href="${pageContext.request.contextPath}/login.jsp"
                   		 	</c:if>
                   		 	
                   		 >
                   		 <span>借书</span></a></li>
                    </ul>

                </div><!-- 课程面板 -->
                <!-- 课程章节 -->
              <iframe name="classVideo" src="" height="700" width="845" scrolling="yes"></iframe>
                <!-- 课程章节 end -->
            </div><!--content end-->
				
			</div>
        <div class="clear"></div>
    </div>
    <!-- 视频介绍 -->
    <div id="js-video-wrap" class="video pop-video" style="display:none">
        <div class="video_box" id="js-video" value="${bookVo.summary}"></div>
        <a href="javascript:;" class="pop-close icon-close"></a>
    </div>
</div>
	

</body>
</html>