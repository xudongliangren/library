<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>充值界面</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/frame/layui/css/layui.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/frame/static/css/style.css">
<link rel="icon" href="<%=request.getContextPath() %>/frame/static/image/code.png">
<!-- css links -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
<link href="css/slider.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="css/facultystyle.css" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
<!-- /css links -->
<link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800' rel='stylesheet' type='text/css'>
<script src="js/SmoothScroll.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
</head>
<style>
        input::-webkit-input-placeholder{
            color:red;
        }
        input::-moz-placeholder{   /* Mozilla Firefox 19+ */
            color:red;
        }
        input:-moz-placeholder{    /* Mozilla Firefox 4 to 18 */
            color:red;
        }
        input:-ms-input-placeholder{  /* Internet Explorer 10-11 */ 
            color:red;
        }
    </style>
<body >
<form class="layui-form" method="post" action="" onsubmit="return false;">
	<div class="socialicons">
		<div class="container" >	    
			<div class="col-md-4 col-sm-4 middle" style="text-align:center;padding-top:200px;">	
				<h3>充值中心</h3>	
		        <div class="layui-input-inline">
		            <input type="text" name="wallet" placeholder="请输入充值金额" class="layui-input" id="wallet">
		        </div>
		        <div class="layui-input-inline login-btn">
		        	<button class="layui-btn" id="submit" onclick="addWallet();"> 充值</button>
		        </div>
		    </div>	
		</div>
	</div>
</form>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
	function addWallet(){
		var wallet = $("#wallet").val();
		
		$.ajax ({
			url:"${pageContext.request.contextPath}/user/addWallet",
			data : {
				wallet : wallet,
			},
			dataType: "json",
			success:function(data){
				if(data.msg==1){
					alert("充值成功！本次充值"+data.wallet+"元.");
					window.location.href = "${pageContext.request.contextPath}/user/selectUserVoById";
				}else{
					alert("充值失败！");
					window.location.href = "${pageContext.request.contextPath}/user/selectUserVoById";
				}	
			
			}
		})
	}

</script>

</body>
</html>
