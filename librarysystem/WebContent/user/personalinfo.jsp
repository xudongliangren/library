<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="ace ace-card-on ace-tab-nav-on ace-main-nav-on ace-sidebar-on" data-theme-color="#c0e3e7">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>个人信息</title>
    <meta name="description" content="">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

    <!-- Icon Fonts -->
    <link href="fonts/icomoon/style.css" rel="stylesheet">

    <!-- Styles -->
    <link href="js/plugins/highlight/solarized-light.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <!-- Modernizer -->
    <script type="text/javascript" src="js/vendor/modernizr-3.3.1.min.js"></script>
  </head>
  <body>
     <div class="ace-wrapper">
         <header id="ace-header" class="ace-container-shift ace-logo-in ace-head-boxed ace-nav-right">
             <div class="ace-head-inner">
                 <div class="ace-head-container ace-container">
                     <div class="ace-head-row">                                             
                         <div id="ace-head-col3" class="ace-head-col text-right">
                             <button id="ace-sidebar-btn" class="">
                                 
                             </button>
                         </div>
                     </div>
                 </div><!-- .ace-container -->
             </div><!-- .ace-head-inner -->
        </header><!-- #ace-header -->  

        <article id="ace-card" class="ace-card bg-primary">
			<div class="ace-card-inner text-center">
				<!-- <img class="avatar avatar-125" src="img/uploads/avatar/avatar-195x195.png" width="125" height="125" alt="" >	 -->							
			</div>
			<footer class="ace-card-footer">
				<a class="btn btn-lg btn-block btn-thin btn-upper " href="#">完善自我信息</a>
				<hr>
				<a class="btn btn-lg btn-block btn-thin btn-upper " href="#">充值</a>
			</footer>
		</article><!-- #ace-card -->
        <div id="ace-content" class="ace-container-shift">
            <div class="ace-container">                              
                <div class="ace-paper-stock">
                    <main class="ace-paper clearfix">
                        <div class="ace-paper-cont clear-mrg">
						
						<!-- START: PAGE CONTENT -->
    <div class="padd-box clear-mrg">
        

        <section class="section brd-btm">
            <div class="row">
                <div class="col-sm-6 clear-mrg">
                    <h2 class="title-thin text-muted">个人信息</h2>

                    <dl class="dl-horizontal clear-mrg">
                        <dt class="text-upper">姓名</dt>
                        <dd>${user.username}</dd>

                        <dt class="text-upper">密码</dt>
                        <dd>${user.pwd}</dd>
                        
                        <dt class="text-upper">电话</dt>
                        <dd>${user.phone}</dd>
                        
						<dt class="text-upper">身份证</dt>
                        <dd>${user.idcard}</dd>

                    </dl>
                </div><!-- .col-sm-6 -->
                
                <div class="col-sm-6 clear-mrg">
                    <h2 class="title-thin text-muted">账户信息</h2>

                    <div class="progress-bullets ace-animate" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10">              
                       <dl class="dl-horizontal clear-mrg">
                        <dt class="text-upper">账户余额</dt>
                        <dd>${user.wallet}</dd>    
                        </dl>              
                    </div>                 
                </div><!-- .col-sm-6 -->         
            </div><!-- .row -->
        </section><!-- .section -->
        <section class="section brd-btm">
            <div class="row">
                <div class="col-sm-12 clear-mrg">
                    <h2 class="title-thin text-muted">借书记录</h2>

                    <ul class="icon-list icon-list-col3 clearfix">
                        <li><span class="ace-icon ace-icon-music"></span> 1</li>                                                                    
                    </ul>
                    <ul class="icon-list icon-list-col3 clearfix">                     	
                   		<li><span class="ace-icon ace-icon-blog"></span> 2</li>                                     
                    </ul>
                     <ul class="icon-list icon-list-col3 clearfix">                     	
                   		<li><span class="ace-icon ace-icon-music"></span> 32</li>                                     
                    </ul>
                </div>
            </div>
        </section><!-- .section -->

        
    </div><!-- .padd-box -->
<!-- END: PAGE CONTENT -->
						
                </div><!-- .ace-paper-cont -->
            </main><!-- .ace-paper -->
        </div><!-- .ace-paper-stock -->

        </div><!-- .ace-container -->
    </div><!-- #ace-content -->

    <div id="ace-sidebar">
		<button id="ace-sidebar-close" class="btn btn-icon btn-light btn-shade">
			<span class="ace-icon ace-icon-close"></span>
		</button>

		
	</div><!-- #ace-sidebar -->
    <!-- Triangle Shapes -->
    <svg id="ace-bg-shape-1" class="hidden-sm hidden-xs" height="519" width="758">
        <polygon points="0,455,693,352,173,0,92,0,0,71" style="fill:#d2d2d2;stroke:purple;stroke-width:0; opacity: 0.5">
    </svg>

    <svg id="ace-bg-shape-2" class="hidden-sm hidden-xs" height="536" width="633">
        <polygon points="0,0,633,0,633,536" style="fill:#c0e3e7;stroke:purple;stroke-width:0" />
    </svg>
</div><!-- .ace-wrapper -->

<!-- Scripts -->
<script type="text/javascript" src="js/vendor/jquery-1.12.4.min.js"></script>


<!---<script type="text/javascript" src="http://ditu.google.cn/maps/api/js?key=AIzaSyDiwY_5J2Bkv2UgSeJa4NOKl6WUezSS9XA"></script>--->
<script type="text/javascript" src="js/plugins/highlight/highlight.pack.js"></script>
<script type="text/javascript" src="js/plugins/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="js/plugins/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/plugins/progressbar.min.js"></script>
<script type="text/javascript" src="js/plugins/slick.min.js"></script>

<script type="text/javascript" src="js/options.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
