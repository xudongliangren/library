<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<title>single</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="My Play Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel='stylesheet' type='text/css' media="all" />
<!-- //bootstrap -->
<link href="${pageContext.request.contextPath}/css/dashboard.css" rel="stylesheet">
<!-- Custom Theme files -->
<link href="${pageContext.request.contextPath}/css/style.css" rel='stylesheet' type='text/css' media="all" />
<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
<!--start-smoth-scrolling-->
<!-- fonts -->
<link href='http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.useso.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<!-- //fonts -->
<style>
       #box{
            width: 300px;
            height: 420px;
            margin: 0 auto;
            
        }
        #box img{
            width: 100%;
            height: 100%;

            
        }
    </style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    
    function addComment(){
		var content = $("#content").val();
		var bookid = $("#bookid").val();
		$.ajax({
			url : "${pageContext.request.contextPath}/user/addComment",
			data : {
				content : content,
				bookid : bookid,
			},
			dataType : "json",
			success : function(data){
				if(data.msg==1){
					alert("评论成功！");
					window.location.href = "${pageContext.request.contextPath}/user/selectAllComment?bookid="+bookid;
				}else{
					alert("评论失败！");
				}
			}
	})
	}
    </script>
</head>
<body>
	<div id="box">	
		<img src="${book.resource}">
	</div>
   
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="show-top-grids">
				<div class="col-sm-8 single-left">

				
					<div class="all-comments">
						<div class="all-comments-info">
							<a href="#">All Comments (${commentList.size()})</a>
							<div class="box">
								<c:if test="${sessionScope.user!=null}">
									<form action="${pageContext.request.contextPath}/user/addComment" method="post">
										<textarea name="content" id="content" placeholder="评论" required=" "></textarea>
										<input type="hidden" name="bookid" id="bookid" value="${book.bookid}" >
										<input type="button" onclick="addComment();" value="发表">
										<div class="clearfix"> </div>
									</form>
								</c:if>
								<c:if test="${sessionScope.user==null}">
									<a href="${pageContext.request.contextPath}/login.jsp">登录</a>
	                                |<a href="${pageContext.request.contextPath}/register.jsp">注册</a>
								</c:if>
								
							</div>
							
						</div>
						
						<div class="media-grids">
							<c:forEach items="${commentList}" var="comment" varStatus="status">
								<div class="media">
									
									<div class="media-left">
										<a href="#">
											
										</a>
									</div>
									<div class="media-body">
										<p>评论${status.index+1}:${comment.content}</p>
										<span>评论人 :<a>${comment.username}</a></span>
									</div>
								</div>
							</c:forEach>
							
						</div>
					</div>
				</div>
				
				<div class="clearfix"> </div>
			</div>
			
		</div>
		<div class="clearfix"> </div>
	<div class="drop-menu">
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu4">
		  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Regular link</a></li>
		  <li role="presentation" class="disabled"><a role="menuitem" tabindex="-1" href="#">Disabled link</a></li>
		  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another link</a></li>
		</ul>
	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
  </body>
</html>