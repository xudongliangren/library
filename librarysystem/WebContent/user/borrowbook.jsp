<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>借书</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/frame/layui/css/layui.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/frame/static/css/style.css">
    <link rel="icon" href="<%=request.getContextPath()%>/frame/static/image/code.png">
  <script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript">
		function borrowbook(){
			var bookid = $("#bookid").val();
			var daylong = $("#daylong").val();
			var wallet = $("#wallet").val();
			if(daylong=="" || daylong==0){
				alert("请输入借书天数");
				return;
			}
			if(wallet == 0 || wallet<0){
				alert("账户已欠费，请交费后再使用！");
				return;
			}
			
			$.ajax({
				url : "${pageContext.request.contextPath}/user/borrowBook",
				data : {
					bookid : bookid,
					daylong : daylong,
				},
				dataType : "json",
				success : function(data){
					if(data.msg==1){
						alert("借书成功！");
						window.location.href = "${pageContext.request.contextPath}/user/selectUserVoById";
					}else{
						alert("借书失败！");
						window.location.href = "${pageContext.request.contextPath}/user/selectUserVoById";
					}
				}
			})
		}
</script>
</head>

<body class="body">

<form method="post" class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/user/borrowBook">
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>

        <div class="layui-input-inline">
            <input type="text" id="username" name="username" value="${user.username}" lay-verify="required" placeholder="请输入" autocomplete="off"
                disabled="disabled"   class="layui-input">
        </div>
        
    </div>

	<div class="layui-form-item">
        <label class="layui-form-label">书籍</label>

        <div class="layui-input-inline">
            <input type="text" id="bookname" name="bookname" placeholder="${book.bookname}" value="${book.bookname}"
            	disabled="disabled"	autocomplete="off" class="layui-input">
            <input type="hidden" id="bookid" name="bookid" value="${book.bookid}" >
        </div>

    </div>
	<div class="layui-form-item">
        <label class="layui-form-label">账户余额</label>

        <div class="layui-input-inline">
            <input type="text" name="wallet" id="wallet" lay-verify="required" value="${user.wallet }"
            	disabled="disabled"	placeholder="请输入" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">借书天数</label>

        <div class="layui-input-inline">
            <input type="text" name="daylong" id="daylong" lay-verify="required" placeholder="请输入" autocomplete="off"
                   class="layui-input">
			<div class="layui-form-mid layui-word-aux">请务必填写</div>              
        </div>
    </div>
    <div class="layui-form-item">
        <input class="layui-btn" onclick="borrowbook();" value="借书" />
    </div>
</form>

<script src="../frame/layui/layui.js" charset="utf-8"></script>

</body>
</html>