<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/framework.tld" prefix="mayi"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js" lang="zxx">
<head>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	//分页查询
	function jump(curPage) {
		$("#curPage").val(curPage);//修改隐藏域的页码
		//还可以进行 form的 提交的url
		$("#form1").action = "${pageContext.request.contextPath }/user/selectBookByTypeOrName";
		//提交form表单
		$("#form1").submit();
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Page</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.jpg">
    
    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/responsive.css">
    <!-- Style customizer (Remove these two lines please) -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style-customizer.css">
    <link href="#" data-style="styles" rel="stylesheet">
    
    <!-- Modernizr JS -->
    <script src="${pageContext.request.contextPath}/js/vendor/modernizr-2.8.3.min.js"></script>
    <style>
       #box1{
            width: 350px;
            height: 800px;
            margin: 0 auto;
            
        }
            
        }
  
       #box2{
            width: 320px;
            height: 750px;
            margin: 0 auto;
            
        }
        #box2 img{
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  
    <!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Start of header area -->
        <header class="header-area">
            <div class="header-top navy-bg ptb-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="header-top-info">
                               <ul>
                                    <li>
                                        <a href="#">
                                            <i class="icofont icofont-ui-call"></i>
                                           	 管理员电话  183 3681 2103
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icofont icofont-envelope"></i>
                                           	管理员邮箱 1175816946@qq.com
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 hidden-xs">
                            <div class="header-top-right f-right">
                                
                                <div class="header-top-language f-left">
                                    <ul>
                                    	<c:if test="${sessionScope.user!=null}">
	                                        <li><a href="#" data-toggle="dropdown">关于个人<i class="icofont icofont-simple-down"></i></a>
	                                            <ul class="dropdown-menu">
	                                                <li><a href="${pageContext.request.contextPath}/user/selectUserVoById">个人信息</a></li>
	                                                <li><a href="${pageContext.request.contextPath}/login/loginOut2">注销</a></li>
	                                            </ul>
	                                        </li>
	                                     </c:if>
	                                     <c:if test="${sessionScope.user==null}">
	                                     	<a href="${pageContext.request.contextPath}/user/selectUserVoById">登录</a>
	                                     	|<a href="${pageContext.request.contextPath}/register.jsp">注册</a>
	                                     </c:if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom stick-h2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            
                        </div>
                        <div class="col-md-8 hidden-sm hidden-xs">
                            <div class="menu-area f-right">
                            	<form id="form1" method="post" action="${pageContext.request.contextPath }/user/selectBookByTypeOrName"">
                                <nav>
                                     <ul>
										<li><select name="typeid" id="typeid">
												<option value="0"></option>
												<option <c:if test="${book.typeid==1}">selected="selected"</c:if> value="1" >东方玄幻</option>
												<option <c:if test="${book.typeid==2}">selected="selected"</c:if> value="2">西方文学</option>
												<option <c:if test="${book.typeid==3}">selected="selected"</c:if> value="3">世界经典</option>
												<option <c:if test="${book.typeid==4}">selected="selected"</c:if> value="4">未来科技</option>
										</select></li>
										<li>
                                       		
						        	 			<input type="text" id="bookname" name="bookname" placeholder="输入书名" value="${book.bookname}">        	
						        				<!-- 隐藏域 -->
												<input type="hidden" id="curPage" name="curPage" value="">
						        			
						        		</li>
						        		<li><input type="button" onclick="javascript:jump(1)" value="搜索" ></li>
						        		
                                    </ul>
                                    </nav>
                                   </form>
                            </div>
                           
                            
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- mobile-menu-area start（               未知区域                    ） -->
        <div class="mobile-menu-area">
            <div class="container">
                <div class="row">
                    <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                    <li class="active"><a href="index.html">HOME </a>
                                        <ul>
                                            <li><a href="text-animation-1.html">home animated text 1</a></li>
                                            <li><a href="text-animation-2.html">home animated text 2</a></li>
                                            <li><a href="text-animation-3.html">home animated text 3</a></li>
                                            <li><a href="text-animation-4.html">home animated text 4</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about-us.html">ABOUT US  </a></li>
                           			<li><a href="#">shortcode <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                        <ul>
                                            <li><a href="#">shortcode style 1</a>
                                                <ul>
                                                    <li><a href="shortcode-alert.html">alert</a></li>
                                                    <li><a href="shortcode-blog.html">blog</a></li>
                                                    <li><a href="shortcode-buttons.html">buttons</a></li>
                                                    <li><a href="shortcode-countdown.html">countdown</a></li>
                                                    <li><a href="shortcode-counter.html">counter</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">shortcode style 2</a>
                                                <ul>
                                                    <li><a href="shortcode-course.html">course</a></li>
                                                    <li><a href="shortcode-event.html">event</a></li>
                                                    <li><a href="shortcode-map.html">map</a></li>
                                                    <li><a href="shortcode-progressbar.html">progressbar</a></li>
                                                    <li><a href="shortcode-service.html">service</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">shortcode style 3</a>
                                                <ul>
                                                    <li><a href="shortcode-social-icon.html">social icon</a></li>
                                                    <li><a href="shortcode-tab.html">tab</a></li>
                                                    <li><a href="shortcode-team.html">team</a></li>
                                                    <li><a href="shortcode-testimonial.html">testimonial</a></li>
                                                    <li><a href="shortcode-testimonial-2.html">testimonial 2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">structure <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                        <ul>
                                            <li><a href="#">Header</a>
                                                <ul>
                                                    <li><a href="header-1.html">Header One</a></li>
                                                    <li><a href="header-2.html">Header Two</a></li>
                                                    <li><a href="header-3.html">Header Three</a></li>
                                                    <li><a href="header-4.html">Header Four</a></li>
                                                    <li><a href="header-5.html">Header Five</a></li>
                                                    <li><a href="header-6.html">Header Six</a></li>
                                                    <li><a href="header-7.html">Header Seven</a></li>
                                                    <li><a href="header-8.html">Header Eight</a></li>
                                                    <li><a href="header-9.html">Header nine</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Footer</a>
                                                <ul>
                                                    <li><a href="footer-1.html">1</a></li>
                                                    <li><a href="footer-2.html">2</a></li>
                                                    <li><a href="footer-3.html">3</a></li>
                                                    <li><a href="footer-4.html">4r</a></li>
                                                    <li><a href="footer-5.html">5ive</a></li>
                                                    <li><a href="footer-6.html">6x</a></li>
                                                    <li><a href="footer-7.html">7en</a></li>
                                                    <li><a href="footer-8.html">8t</a></li>
                                                    <li><a href="footer-9.html">9e</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Page Title</a>
                                                <ul>
                                                    <li><a href="page-title-default.html"> Default Title</a></li>
                                                    <li><a href="page-title-bg-fixed.html"> Background Fixed</a></li>
                                                    <li><a href="page-title-dark.html"> Dark Title</a></li>
                                                    <li><a href="page-title-no-bg.html"> No Background</a></li>
                                                    <li><a href="page-title-pattern.html"> Pattern Title</a></li>
                                                    <li><a href="page-title-solid-bg-color.html">solid Title</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">pages <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                        <ul>
                                            <li><a href="about-us.html">about us</a></li>
                                            <li><a href="contact.html">contact</a></li>
                                            <li><a href="course.html">course</a></li>
                                            <li><a href="course-details.html">course details</a></li>
                                            <li><a href="events.html">events</a></li>
                                            <li><a href="events-details.html">events details</a></li>
                                            <li><a href="news-details.html">news details</a></li>
                                            <li><a href="news-page.html">news page</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="news-page.html">NEWS  </a></li>
                                    <li><a href="contact.html">CONTACT </a></li>
                                </ul>
                            </nav>
                        </div>					
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-menu-area end -->
        <!-- End of header area -->
        <section class="breadcrumbs-area bg-3 ptb-110 bg-opacity bg-relative">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="breadcrumbs">
                            <h2 class="page-title"></h2>
                            <ul>
                                <li>
                                    <a class="active" href="#">最新最热的书籍都在这！！！</a>
                                </li>
                              
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Start page content -->
        <section class="news-page-area ptb-110">
            <div class="container">
                <div class="row">
                	<c:forEach var="book" items="${page.date}" varStatus="status">
                		
	                    <div class="col-md-4 col-sm-6" id="box1">
	                        <div class="news-are" id="box2">
	                            <div class="news-img">
	                            	
	                               		<img  alt="${book.bookname}" src="${book.resource}">
	                                
	                                <div class="news-date navy-bg">
	                                    
	                                    <div class="blog-meta for-news">
	                                        <span class="published3">
	                                            <a href="#">
	                                                <i class="icofont icofont-eye"></i> ${book.clicknum}
	                                            </a>
	                                        </span>
	                                        <span class="published4">
	                                            <a href="${pageContext.request.contextPath}/user/selectAllComment?bookid=${book.bookid}">
	                                                <i class="icofont icofont-comment"></i> ${book.commentNum}
	                                            </a>
	                                        </span>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="img-text gray-bg">
	                                <h3><a href="#">《${book.bookname}》</a></h3>
	                                <p>${book.summary}</p>
	                                <a class="button extra-small" href="${pageContext.request.contextPath}/user/selectBookById?bookid=${book.bookid}">
	                                    <span>查看</span>
	                                </a>
	                            </div>
	                        </div>
	                    </div>
	                     
               		</c:forEach>
               
                </div>
                <div align="center">
					<p>
						<c:if test="${page.curPage == 1}">首页</c:if>
						<c:if test="${page.curPage > 1}">
							<a href="javascript:jump(1)">首页</a>
						</c:if>
						<c:if test="${page.curPage == 1 }">上一页</c:if>
						<c:if test="${page.curPage > 1 }">
							<a href="javascript:jump(${page.curPage-1 })">上一页</a>
						</c:if>
						<c:if test="${page.curPage == page.totalPage }">下一页</c:if>
						<c:if test="${page.curPage < page.totalPage }">
							<a href="javascript:jump(${page.curPage+1 })">下一页</a>
						</c:if>
						<c:if test="${page.curPage == page.totalPage }">末页</c:if>
						<c:if test="${page.curPage < page.totalPage }">
							<a href="javascript:jump(${page.totalPage })">末页</a>
						</c:if>
						共${page.totalPage }页 当前页${page.curPage } 总共${page.rows }条数据
					</p>
				</div>
            </div>
        </section>
 
        
  
        <!--style-customizer end -->
    </div>
    <!-- Body main wrapper end -->
    
    
    
    
    <!-- Placed js at the end of the document so the pages load faster -->
    <!-- jquery latest version -->
    <script src="${pageContext.request.contextPath}/js/vendor/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <!-- ajax-mail JS
    ============================================ -->		
    <script src="${pageContext.request.contextPath}/js/ajax-mail.js"></script>
    <!-- All js plugins included in this file. -->
    <script src="${pageContext.request.contextPath}/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>

</body>

</html>
