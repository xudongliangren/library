<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js" lang="zxx">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/images/favicon.jpg">
    
    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/responsive.css">
    <!-- Style customizer (Remove these two lines please) -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style-customizer.css">
    <link href="#" data-style="styles" rel="stylesheet">
    
    <!-- Modernizr JS -->
    <script src="${pageContext.request.contextPath}/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  
    <!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Start of header area -->
        <header class="header-area">
            <div class="header-top navy-bg ptb-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="header-top-info">
                               <ul>
                                    <li>
                                        <a href="#">
                                            <i class="icofont icofont-ui-call"></i>
                                           	 管理员电话  183 3681 2103
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="icofont icofont-envelope"></i>
                                           	管理员邮箱 1175816946@qq.com
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 hidden-xs">
                            <div class="header-top-right f-right">
                                
                                <div class="header-top-language f-left">
                                    <ul>
                                        <li><a href="#" data-toggle="dropdown">关于个人<i class="icofont icofont-simple-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">个人信息</a></li>
                                                <li><a href="#">账户余额</a></li>
                                              
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom stick-h2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            
                        </div>
                        <div class="col-md-8 hidden-sm hidden-xs">
                            <div class="menu-area f-right">
                                <nav>
                                     <ul>
                                        <li><a href="index.htm">古代名著</a></li>
                                        <li><a href="about.html">玄幻热血</a></li>
										<li><a href="blog.html">外国文学</a></li>
										
                                        <li>
                                       		<form id="form1" method="post" action="/admin/selectTeacherByContition">
						        	 			<input type="text" name="teaname" value="搜你所想">	
						        	 			<input type="submit" name="Submit" class="btn" value="搜索" />					           	
						        			</form>
						        		</li>
                                    </ul>
                                   
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
   
        <!-- mobile-menu-area end -->
        <!-- End of header area -->
        <!-- Start of slider area -->
        <div class="slider-area">
            <div class="slider-active">
                <div class="slider-all">
                    <img src="${pageContext.request.contextPath}/images/slider/1.jpg" alt="">
                    <div class="slider-text-wrapper">
                        <div class="table">
                            <div class="table-cell">
                                <div class="slider-text animated">
                                   
                                    <a class="button extra-small mb-20" href="#">
                                        <span>尊敬的用户，欢迎您来到文学的海洋！</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div class="slider-all">
                    <img src="${pageContext.request.contextPath}/images/slider/1.jpg" alt="">
                    <div class="slider-text-wrapper">
                        <div class="table">
                            <div class="table-cell">
                                <div class="slider-text animated">
                                    <h1>Education For Everyone</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor <br> aliqua.  minim veniam, quis nostrud exercitation ullamco </p>
                                    <a class="button extra-small mb-20" href="#">
                                        <span>Apply Now</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
       

        
        <!--style-customizer start -->
        <div class="style-customizer closed">
            <div class="buy-button">
                <a href="" class="customizer-logo"><img src="${pageContext.request.contextPath}/images/logo/logo.png" alt="Theme Logo"></a>
                <a class="opener" href="#"><i class="fa fa-cog fa-spin"></i></a> <a class="button button-border" href="#">感受文字的力量！</a> 
            </div>
            <div class="clearfix content-chooser">
                <h3>Layout Options</h3>
               <h3>学习天地</h3>
                <p>还不快来看看</p>
                <ul class="patternChange main-bg-change clearfix">
                    <li class="main-bg-5" data-style="main-bg-5" title="Background 5"> <img src="${pageContext.request.contextPath}/images/customizer/bodybg/05.jpg" alt=""></li>
                    <li class="main-bg-6" data-style="main-bg-6" title="Background 6"> <img src="${pageContext.request.contextPath}/images/customizer/bodybg/06.jpg" alt=""></li>
                    <li class="main-bg-4" data-style="main-bg-4" title="Background 4"> <img src="${pageContext.request.contextPath}/images/customizer/bodybg/04.jpg" alt=""></li>
            
                </ul>
                <h3>走进玄幻的大门</h3>
                <p>看你所想</p>
                <ul class="styleChange clearfix">
                   
                    <li class="color-1" title="color-1" data-style="color-1"><p>玄幻</p></li>
                    <li class="color-3" title="color-3" data-style="color-3"><p>冒险</p></li>
                    <li class="color-4" title="color-4" data-style="color-4"><p>历史</p></li>
                    <li class="color-5" title="color-5" data-style="color-5"><p>文学</p></li>
                 
                </ul>
                <h3>文学乐园</h3>
                <p>哈哈哈哈哈</p>
                <ul class="patternChange clearfix">
                    <li class="pattern-1" data-style="pattern-1" title="pattern-1"></li>
                    <li class="pattern-2" data-style="pattern-2" title="pattern-2"></li>
                    <li class="pattern-3" data-style="pattern-3" title="pattern-3"></li>
  
                </ul>
                
                <ul class="resetAll">
                    <li><a class="button button-border button-reset" href="#">Reset All</a></li>
                </ul>
            </div>
        </div>
        <!--style-customizer end -->
    </div>
    <!-- Body main wrapper end -->
    
    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- jquery latest version -->
    <script src="${pageContext.request.contextPath}/js/vendor/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <!--  ajax-mail.js  -->	
    <script src="${pageContext.request.contextPath}/js/ajax-mail.js"></script>
    <!-- All js plugins included in this file. -->
    <script src="${pageContext.request.contextPath}/js/plugins.js"></script>
    <script src="${pageContext.request.contextPath}/js/main.js"></script>

</body>

</html>
