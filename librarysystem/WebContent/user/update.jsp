<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>修改</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/frame/layui/css/layui.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/frame/static/css/style.css">
    <link rel="icon" href="<%=request.getContextPath()%>/frame/static/image/code.png">
</head>

<body class="body">

<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/user/updateUser">
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>

        <div class="layui-input-inline">
            <input type="text" name="username" id="username" value="${user.username}" autocomplete="off"
                   class="layui-input">				
        </div>
    </div>

	<div class="layui-form-item">
        <label class="layui-form-label">密码</label>

        <div class="layui-input-inline">
            <input type="text" name="pwd" id="pwd" value="${user.pwd}" autocomplete="off"
             class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">电话</label>

        <div class="layui-input-inline">
            <input type="text" name="phone" id="phone" value="${user.phone}" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">身份证</label>

        <div class="layui-input-inline">
            <input type="text" name="idcard" id="idcard" value="${user.idcard}" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <button class="layui-btn"  type="submit">修改</button>
    </div>
</form>

<script src="<%=request.getContextPath()%>/frame/layui/layui.js" charset="utf-8"></script>

</body>
</html>