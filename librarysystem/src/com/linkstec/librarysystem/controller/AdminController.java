package com.linkstec.librarysystem.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.pojo.Location;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.service.AdminService;
import com.linkstec.librarysystem.util.Page;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.vo.UserBorrowVo;


/**     
 * @Description:TODO管理员的controller
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:30:48     
 */
@Controller
@RequestMapping("admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	/**     
	 * @Description:TODO分页查询所有用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:42:37    
	 * @param page
	 * @return      
	 */  
	@RequestMapping("showAllUsers")
	public ModelAndView showAllUsers(Page page){
		ModelAndView mv = new ModelAndView();
		try {
			page=adminService.showAllUsers(page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("page", page);
		mv.addObject("username", "");
		mv.setViewName("usermanage");
		return mv;
		
	}
	/**     
	 * @Description:TODO根据用户名分页模糊查询用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:50:19    
	 * @param username
	 * @param page
	 * @return      
	 */  
	@RequestMapping("selectUsersByName")
	public ModelAndView selectUsersByName(String username,Page page){
		ModelAndView mv = new ModelAndView();
		try {
			page = adminService.selectUsersByName(username,page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("page", page);
		mv.addObject("username", username);
		mv.setViewName("usermanage");
		return mv;
	}
	/**     
	 * @Description:TODO变更用户启用禁用状态
	 * @author: duyi
	 * @date:   2018年11月26日 下午9:32:32    
	 * @param userid
	 * @param userstate
	 * @return      
	 */  
	@RequestMapping("changeUserstate")
	public String changeUserstate(int userid,int userstate){
		if(userstate==1){
		userstate=0;
		}else{
			userstate=1;
		}
		try {
			adminService.changeUserstate(userid,userstate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/showAllUsers";
	}
	/**     
	 * @Description:TODO查看用户借书历史
	 * @author: duyi
	 * @date:   2018年11月23日 下午3:21:54    
	 * @param userid
	 * @return      
	 */  
	@RequestMapping("showUserLog")
	public ModelAndView showUserLog(int userid){
		ModelAndView mv = new ModelAndView();
		List<UserBorrowVo>borrowlist = new ArrayList<UserBorrowVo>();
		try {
			borrowlist = adminService.showUserLog(userid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("borrowlist",borrowlist);
		mv.setViewName("userborrowlog");
		return mv;
	}
	/**     
	 * @Description:TODO查询出该用户的信息并跳转到修改用户信息页面
	 * @author: duyi
	 * @date:   2018年11月24日 下午2:08:11    
	 * @param userid
	 * @return      
	 */  
	@RequestMapping("toUpdateUser")
	public ModelAndView toUpdateUser(int userid){
		ModelAndView mv = new ModelAndView();
		User user = new User();
		try {
			user = adminService.toUpdateUser(userid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("user", user);
		mv.setViewName("");
		return mv;
	}
	/**     
	 * @Description:TODO修改用户信息
	 * @author: duyi
	 * @date:   2018年11月24日 下午2:38:40    
	 * @param user
	 * @return      
	 */  
	@RequestMapping("updateUser")
	public String updateUser(User user){
		try {
			adminService.updateUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/showAllUsers";
	}
	/**     
	 * @Description:TODO根据userid查看该用户的所有评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:20:49    
	 * @param userid
	 * @return      
	 */  
	@RequestMapping("checkUserComment")
	public ModelAndView checkUserComment(int userid){
		ModelAndView mv = new ModelAndView();
		List<CommentVo>list = null;
		try {
			list = adminService.checkUserComment(userid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("commentlist", list);
		mv.setViewName("usercomment");
		return mv;
	}
	/**     
	 * @Description:TODO删除该用户的评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:36:16    
	 * @param commentid
	 * @return      
	 */  
	@RequestMapping("deleteUserComment")
	public String deleteUserComment(int[] commentids){
		try {
			adminService.deleteComment(commentids);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return"redirect:/amdin/checkUserComment";
	}
	/**     
	 * @Description:TODO分页展示所有书籍
	 * @author: duyi
	 * @date:   2018年11月24日 下午4:23:21    
	 * @param page
	 * @return      
	 */  
	@RequestMapping("showAllBooks")
	public ModelAndView showAllBooks(Page page){
		ModelAndView mv = new ModelAndView();
		try {
			page = adminService.showAllBooks(page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("page", page);
		mv.addObject("bookname", "");
		mv.setViewName("bookmanage");
		return mv;
	}
	/**     
	 * @Description:TODO根据书名或类别名分页模糊查询
	 * @author: duyi
	 * @date:   2018年11月25日 上午12:22:12    
	 * @param selectk
	 * @param username
	 * @param page
	 * @return      
	 */  
	@RequestMapping("selectBooksByName")
	public ModelAndView selectBooksByName(String selectk,String bookname,Page page){
		ModelAndView mv = new ModelAndView();
		try {
			page = adminService.selectBooksByName(bookname,page);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("page", page);
		mv.addObject("bookname", bookname);
		mv.setViewName("bookmanage");
		return mv;
	}

	/**     
	 * @Description:TODO跳转到新增书籍页面
	 * @author: duyi
	 * @date:   2018年11月26日 上午11:34:34    
	 * @return      
	 */  
	@RequestMapping("toAddNewBook")
	public ModelAndView toAddNewBook(){
		ModelAndView mv = new ModelAndView();
		List<BookType>list1 = null;
		List<Location>list2 = null;
		try {
			list1 = adminService.selectAllBookType();
			list2 = adminService.selectAllLocation();
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("btlist", list1);
		mv.addObject("locationlist", list2);
		mv.setViewName("addbook");
		return mv;
	}
	/**     
	 * @Description:TODO添加新书
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:20:08    
	 * @param bv
	 * @return      
	 */  
	@RequestMapping("addNewBook")
	public String addNewBook(BookVo bv){
		BookType bt = new BookType();
		try {
		//	bt = adminService.selectBookTypeByName(bv.getTypename());
		//	bv.setTypeid(bt.getTypeid());
			bv.setBooksurplus(bv.getBooknum());
			bv.setClicknum(0);
			bv.setBookstate(1);
			adminService.addNewBook(bv);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return"redirect:/admin/showAllBooks";
	}
	/**     
	 * @Description:TODO变更书的上架下架状态
	 * @author: duyi
	 * @date:   2018年11月25日 下午6:17:22    
	 * @return      
	 */  
	@RequestMapping("updateBookState")
	public String updateBookState(int bookid,int bookstate){
		if(bookstate==1){
			bookstate=0;
		}else{
			bookstate=1;
		}
		try {
			adminService.updateBookState(bookid,bookstate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/showAllBooks";
	}
	/**     
	 * @Description:TODO跳转到修改页面，并把用户详细信息展示出来
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:01:57    
	 * @param bookid
	 * @return      
	 */  
	@RequestMapping("toUpdateBook")
	public ModelAndView toUpdateBook(int bookid){
		ModelAndView mv = new ModelAndView();
		BookVo bv = new BookVo();
		List<BookType>list1 = null;
		List<Location>list2 = null;
		try {
			list1 = adminService.selectAllBookType();
			list2 = adminService.selectAllLocation();
			bv = adminService.toUpdateBook(bookid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("btlist", list1);
		mv.addObject("locationlist", list2);
		mv.addObject("bookinfo", bv);
		mv.setViewName("updatebook");
		return mv;
	}
	/**     
	 * @Description:TODO修改书籍信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:54:38    
	 * @param book
	 * @return      
	 */  
	@RequestMapping("updateBook")
	public String updateBook(Book book){
		try {
			adminService.updateBook(book);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return"redirect:/admin/showAllBooks";
	}
	/**     
	 * @Description:TODO查看该书籍的评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午8:21:35    
	 * @param bookid
	 * @return      
	 */  
	@RequestMapping("checkBookComment")
	public ModelAndView checkBookComment(int bookid){
		ModelAndView mv = new ModelAndView();
		List<CommentVo>list = null;
		try {
			list = adminService.checkBookComment(bookid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("commentlist", list);
		mv.setViewName("bookcomment");
		return mv;
	}
	/**     
	 * @Description:TODO删除关于该书籍的评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午8:58:58    
	 * @param commentid
	 * @return      
	 */  
	@RequestMapping("deleteBookComment")
	public String deleteBookComment(int[] commentids){
		try {
			adminService.deleteComment(commentids);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/admin/checkBookComment";
	}

}
