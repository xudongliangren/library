package com.linkstec.librarysystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.service.RegisterService;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:30:13     
 */
@Controller
@RequestMapping("register")
public class RegisterController {
	@Autowired
	private RegisterService registerService;
	
	/**     
	 * @Description:TODO注册新用户
	 * @author: duyi
	 * @date:   2018年11月22日 上午10:19:39    
	 * @param user
	 * @return      
	 */  
	@RequestMapping("registerUser")
	@ResponseBody
	public String registerUser(User user){
		JSONObject json = new JSONObject();
		int reg = 0;
		try {
			reg = registerService.checkUser(user);
			if(reg==0){
				user.setUserstate(1);
				user.setWallet(0);
				registerService.registerUser(user);
				json.put("msg", 1);
			}else{
				json.put("msg", 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	/**     
	 * @Description:TODO跳转到登陆页
	 * @author: duyi
	 * @date:   2018年11月22日 上午11:29:21    
	 * @return      
	 */  
	@RequestMapping("toLogin")
	public String toLogin(){
		return"login";
	}
}
