package com.linkstec.librarysystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.service.UserService;
import com.linkstec.librarysystem.util.Page;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.vo.UserBorrowVo;
import com.linkstec.librarysystem.vo.UserVo;

import sun.net.www.content.text.plain;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:26:22     
 */
@Controller
@RequestMapping("user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	private static Logger log = Logger.getLogger(UserController.class);
	
	/**     
	 * @Description:TODO 查询所有书籍 按热度排序 分页
	 * @author: DongLiang
	 * @date:   2018年11月24日 下午3:51:48    
	 * @param page
	 * @return      
	 */  
	@RequestMapping(value ="selcetAllBook")
	public ModelAndView selcetAllBook(Page page){
		ModelAndView mv = new ModelAndView();
		Book book = new Book();
		book.setBookname("");
		book.setTypeid(0);
		try {
			page = userService.selectAllBook(page);
		} catch (Exception e) {
			log.debug(page.toString());
		}
		mv.addObject("page", page);
		mv.addObject("book", book);
		mv.setViewName("/user/page");
		return mv;
	}
	
	/**     
	 * @Description:TODO 通过书籍类别或书籍名称查询书籍 通过点击量排序 分页
	 * @author: DongLiang
	 * @date:   2018年11月22日 上午10:38:45    
	 * @param page
	 * @return      
	 */  
	@RequestMapping(value="selectBookByTypeOrName")
	public ModelAndView selectBookByTypeOrName(Page page,Book book){
		ModelAndView mv = new ModelAndView();
		try {
			page = userService.selectBookByTypeOrName(page,book);
		} catch (Exception e) {
			log.debug(page.toString());
		}
		mv.addObject("page", page);
		mv.addObject("book", book);
		mv.setViewName("/user/page");
		return mv;
	}
	
	/**     
	 * @Description:TODO 通过图书id获取书籍信息
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:06:17    
	 * @return      
	 */  
	@RequestMapping(value="selectBookById")
	public ModelAndView selectBookById(int bookid){
		ModelAndView mv = new ModelAndView();
		BookVo bookVo = null;
		try {
			bookVo = userService.selectBookVoById(bookid);
		} catch (Exception e) {
			log.debug(bookVo.toString());
		}
		
		mv.addObject("bookVo", bookVo);
		mv.setViewName("/user/bookDetail");
		return mv;
	}
	
	/**     
	 * @Description:TODO 查询所有评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:23:34    
	 * @return      
	 */  
	@RequestMapping(value = "selectAllComment")
	public ModelAndView selectAllComment(int bookid){
		ModelAndView mv = new ModelAndView();
		List<CommentVo> commentList = null;
		BookVo book = null;
		try {
			commentList = userService.selectAllComment(bookid);
			book = userService.selectBookVoById(bookid);
		} catch (Exception e) {
			log.debug(commentList.toString());
		}
		mv.addObject("commentList", commentList);
		mv.addObject("book", book);
		mv.setViewName("/user/commentShow");
		return mv;
	}
	
	/**     
	 * @Description:TODO 添加评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:42:47    
	 * @return      
	 */  
	@RequestMapping(value = "addComment")
	@ResponseBody
	public String addComment(HttpServletRequest req, Comment comment){
		int flag = 0;
		JSONObject json = new JSONObject();
		User user = (User) req.getSession().getAttribute("user");
		comment.setUserid(user.getUserid());
		try {
			flag = userService.addComment(comment);
		} catch (Exception e) {
			log.debug(comment.toString());
		}
		if(flag==1){
			json.put("msg", 1);
		}else{
			json.put("msg", 0);
		}
		return json.toString();
	}
	
	/**     
	 * @Description:TODO 跳转借书页面
	 * @author: DongLiang
	 * @date:   2018年11月26日 上午11:38:57    
	 * @return      
	 */  
	@RequestMapping(value="toBorrowBook")
	public ModelAndView toBorrowBook(int bookid){
		ModelAndView mv = new ModelAndView();
		BookVo bookVo = null;
		try {
			bookVo = userService.selectBookVoById(bookid);
		} catch (Exception e) {
			log.debug(bookVo.toString());
		}
		mv.addObject("book", bookVo);
		mv.setViewName("/user/borrowbook");
		return mv;
	}
	
	/**     
	 * @Description:TODO 借书 
	 * @author: DongLiang
	 * @date:   2018年11月23日 上午11:30:47    
	 * @return      
	 */  
	@RequestMapping(value="borrowBook")
	@ResponseBody
	public String borrowBook(HttpServletRequest req,int bookid,int daylong){
		JSONObject json = new JSONObject();
		User user =(User) req.getSession().getAttribute("user");
		int userid = user.getUserid();
		UserBorrow userborrow = new UserBorrow();
		userborrow.setUserid(userid);
		userborrow.setBookid(bookid);
		userborrow.setDaylong(daylong);
		int flag = 0;
		try {
			flag = userService.borrowBook(userborrow);
		} catch (Exception e) {
			log.debug(user.toString());
		}
		if(flag==1){
			json.put("msg", 1);
		}else{
			json.put("msg", 0);
		}
		return json.toString();
	}
	
	/**     
	 * @Description:TODO 查询用户信息和借书记录
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午2:48:30    
	 * @return      
	 */  
	@RequestMapping(value="selectUserVoById")
	public ModelAndView selectUserVoById(HttpServletRequest req){
		ModelAndView mv = new ModelAndView();
		User user =(User) req.getSession().getAttribute("user");
		List<UserBorrowVo> borrowVoList = null;
		try {
			borrowVoList = userService.selectBorrowByUserId(user.getUserid());
			user = userService.selectUserById(user.getUserid());
		} catch (Exception e) {
			log.debug(borrowVoList.toString());
		}
		req.getSession().setAttribute("user", user);
		mv.addObject("borrowVoList", borrowVoList);
		mv.setViewName("/user/user");
		return mv;
	}
	
	/**     
	 * @Description:TODO 还书
	 * @author: DongLiang
	 * @date:   2018年11月27日 上午10:48:46    
	 * @return      
	 */  
	@RequestMapping(value="returnBook")
	@ResponseBody
	public String returnBook(UserBorrow userBorrow){
		JSONObject json = new JSONObject();
		double money = 0;
		try {
			money = userService.returnBook(userBorrow.getBorrowid());
		} catch (Exception e) {
			log.debug(userService);
		}
		if(money!=0){
			json.put("msg", 1);
			json.put("money", money);
		}else{
			json.put("msg", 0);
		}
		return json.toString();
	}
	
	/**     
	 * @Description:TODO   充值
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午4:10:43    
	 * @return      
	 */  
	@RequestMapping(value="addWallet")
	@ResponseBody
	public String addWallet(HttpServletRequest req ,User user1){
		JSONObject json = new JSONObject();
		User user =(User) req.getSession().getAttribute("user");
		user1.setUserid(user.getUserid());
		int result = 0;
		double wallet = user1.getWallet();
		try {
			result = userService.addWallet(user1);
		} catch (Exception e) {
			log.debug(userService);
		}
		if(result==1){
			json.put("msg", 1);
			json.put("wallet", wallet);
		}else{
			json.put("msg", 0);
		}
		return json.toString();
	}
	
	/**     
	 * @Description:TODO  跳转到充值页
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午4:29:28    
	 * @return      
	 */  
	@RequestMapping(value="toDeposit")
	public String toDeposit(){
		return "/user/deposit";
	}
	
	/**     
	 * @Description:TODO  跳转修改页
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午5:10:18    
	 * @return      
	 */  
	@RequestMapping(value="toUpdateUser")
	public String toUpdateUser(){	
		return "/user/update";
	}
	
	/**     
	 * @Description:TODO  修改个人信息
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午5:21:39    
	 * @param user
	 * @return      
	 */  
	@RequestMapping(value="updateUser")
	public String updateUser(HttpServletRequest req,User user1){
		User user =(User) req.getSession().getAttribute("user");
		user1.setUserid(user.getUserid());
		try {
			userService.changeUser(user1);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "redirect:/user/selectUserVoById";
	}
}





