package com.linkstec.librarysystem.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.linkstec.librarysystem.pojo.Admin;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.service.LoginService;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:13:36     
 */
@Controller
@RequestMapping("login")
public class LoginController {
	@Value("${adminname}")
	public String adminname;
	
	@Value("${adminpassword}")
	private String adminpassword;
	@Autowired
	private LoginService loginService;
	
	/**     
	 * @Description:TODO用户或管理员登陆
	 * @author: duyi
	 * @date:   2018年11月22日 下午2:43:53    
	 * @param req
	 * @param user
	 * @return      
	 */  
	@RequestMapping("sbLogin")
	@ResponseBody 
	public String sbLogin(HttpServletRequest req, User user){
		JSONObject json = new JSONObject();
		HttpSession session = req.getSession();
		User user2 = new User();
		Admin admin =new Admin();
		if(user.getUsername().equals(adminname)
				&&user.getPwd().equals(adminpassword)){
			admin.setAdminname(adminname);
			admin.setAdminpassword(adminpassword);
			session.setAttribute("loginUser", admin);
			json.put("msg", 2);
			}
			else{
				try {
					user2 = loginService.sbLogin(user);
					if(user2!=null&&user2.getUserstate()==1){
						session.setAttribute("user", user2);
						json.put("msg", 1);
					}else{
						json.put("msg", 0);
					}		
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
		return json.toString();
	}
	/**     
	 * @Description:TODO登陆者为用户，跳转到用户首页
	 * @author: duyi
	 * @date:   2018年11月22日 下午2:51:11    
	 * @return      
	 */  
	@RequestMapping("loginUser")
	public String loginUser(){
		return "redirect:/user/selcetAllBook";
	}
	/**     
	 * @Description:TODO登陆者为管理员，跳转到管理员首页
	 * @author: duyi
	 * @date:   2018年11月22日 下午2:54:10    
	 * @return      
	 */  
	@RequestMapping("loginAdmin")
	public String loginAdmin(){
		return"/admin/admin";
	}
	/**     
	 * @Description:TODO退出登录
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:54:49    
	 * @param req
	 * @return      
	 */  
	@RequestMapping(value="loginOut")
	public String loginOut(HttpServletRequest req){
		HttpSession session = req.getSession();
		session.removeAttribute("loginUser");
		return "/login";
	}
	@RequestMapping(value="loginOut2")
	public String loginOut2(HttpServletRequest req){
		HttpSession session = req.getSession();
		session.removeAttribute("user");
		return "/login";
	}
}