package com.linkstec.librarysystem.mapper;

import java.util.List;

import com.linkstec.librarysystem.pojo.BookType;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午8:46:28     
 */
public interface BookTypeMapper {

	/**     
	 * @Description:TODO查询所有booktype信息
	 * @author: duyi
	 * @date:   2018年11月26日 下午2:15:29    
	 * @return      
	 */  
	List<BookType> selectAllBookType();

	/**     
	 * @Description:TODO 根据typeid查询booktype信息
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午1:12:17    
	 * @param typeid
	 * @return      
	 */  
	BookType selectTypeById(int typeid);

}
