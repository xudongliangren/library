package com.linkstec.librarysystem.mapper;

import java.util.List;

import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.vo.CommentVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午8:46:41     
 */
public interface CommentMapper {

	/**     
	 * @Description:TODO 根据书籍id查询所有评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:33:32    
	 * @param bookid
	 * @return      
	 */  
	List<CommentVo> selectAllComment(int bookid);

	/**     
	 * @Description:TODO 添加评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:58:08    
	 * @param comment
	 * @return      
	 */  
	int addComment(Comment comment);

	/**     
	 * @Description:TODO 查询书籍的评论数
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午4:41:20    
	 * @param bookid
	 * @return      
	 */  
	int selectNumByBookId(int bookid);
	
	/**
	 * @Description:TODO根据commentid删除评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:03:35    
	 * @param commentids      
	 */  
	void deleteComment(int[] commentids);

	/**     
	 * @Description:TODO根据userid查看该用户的所有评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:26:06    
	 * @param userid
	 * @return      
	 */  
	List<CommentVo> checkUserComment(int userid);

}
