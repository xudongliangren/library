package com.linkstec.librarysystem.mapper;

import java.util.List;

import com.linkstec.librarysystem.pojo.Location;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午8:46:56     
 */
public interface LocationMapper {

	/**     
	 * @Description:TODO查询所有location信息
	 * @author: duyi
	 * @date:   2018年11月26日 下午2:19:15    
	 * @return      
	 */  
	List<Location> selectAllLocation();

}
