package com.linkstec.librarysystem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.vo.UserBorrowVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午8:47:04     
 */
public interface UserMapper {

	/**     
	 * @Description:TODO注册新用户
	 * @author: duyi
	 * @date:   2018年11月22日 上午10:56:02    
	 * @param user
	 * @return      
	 */  
	void registerUser(User user);

	/**     
	 * @Description:TODO检查用户名是否存在
	 * @author: duyi
	 * @date:   2018年11月22日 上午11:00:53    
	 * @param user
	 * @return      
	 */  
	int checkUser(User user);

	/**     
	 * @Description:TODO验证账号密码
	 * @author: duyi
	 * @date:   2018年11月22日 下午2:25:35    
	 * @param user
	 * @return      
	 */  
	User sbLogin(User user);

	/**     
	 * @Description:TODO分页查询所有用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:30:11    
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<User> showAllUsers(int start, int pageNumber);

	/**     
	 * @Description:TODO查询用户总数
	 * @author: duyi
	 * @param username 
	 * @date:   2018年11月23日 上午11:37:45    
	 * @return      
	 */  
	int selectUserCount();

	/**     
	 * @Description:TODO根据用户名分页模糊查询用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:56:46    
	 * @param username
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  

	List<User> selectUsersByName(@Param("username")String username, int start, int pageNumber);

	/**     
	 * @Description:TODO查看用户借书历史
	 * @author: duyi
	 * @date:   2018年11月23日 下午3:26:04    
	 * @param userid
	 * @return      
	 */  
	List<UserBorrowVo> showUserLog(int userid);

	/**     
	 * @Description:TODO查询出该用户的信息并跳转到修改用户信息页面
	 * @author: duyi
	 * @date:   2018年11月24日 下午2:13:59    
	 * @param userid
	 * @return      
	 */  
	User selectUserById(int userid);

	/**     
	 * @Description:TODO修改用户信息
	 * @author: duyi
	 * @date:   2018年11月24日 下午3:24:22    
	 * @param user      
	 */  
	void updateUser(User user);

	/**     
	 * @Description:TODO模糊查询名字中带有特定字的用户
	 * @author: duyi
	 * @date:   2018年11月24日 下午9:33:40    
	 * @param username
	 * @return      
	 */  
	int selectUserCountByName(@Param("username")String username);

	/**     
	 * @Description:TODO变更用户启用禁用状态
	 * @author: duyi
	 * @date:   2018年11月26日 下午9:35:28    
	 * @param userid
	 * @param userstate      
	 */  
	void changeUserstate(@Param("userid")int userid,@Param("userstate") int userstate);

	/**     
	 * @Description:TODO 扣钱
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午2:44:28    
	 * @param money
	 * @param userid      
	 */  
	void pay(double money, int userid);

	/**     
	 * @Description:TODO   充值
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午4:17:31    
	 * @param userid
	 * @return      
	 */  
	int addWallet(User user);

	/**     
	 * @Description:TODO  修改个人信息
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午5:28:54    
	 * @param user1      
	 */  
	void changeUser(User user1);
}
