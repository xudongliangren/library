package com.linkstec.librarysystem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.vo.BookVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午8:46:04     
 */
public interface BookMapper {
	
	/**     
	 * @Description:TODO 根据通过书籍类别或书籍名称查询书籍
	 * @author: DongLiang
	 * @date:   2018年11月22日 上午11:23:22    
	 * @param book
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<BookVo> selectBookByTypeOrName(@Param("typeid")int typeid ,@Param("bookname")String bookname, @Param("start")int start, @Param("pageNumber")int pageNumber);

	/**     
	 * @Description:TODO 查询符合条件的书的数量
	 * @author: DongLiang
	 * @date:   2018年11月22日 上午11:07:33    
	 * @param book      
	 */  
	int selectBookNum_ByTypeOrName(Book book);

	/**     
	 * @Description:TODO 通过书籍id查询书籍信息
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:13:27    
	 * @param bookid
	 * @return      
	 */  
	BookVo selectBookVoById(int bookid);

	/**     
	 * @Description:TODO分页查询所有图书
	 * @author: duyi
	 * @date:   2018年11月24日 下午4:08:21    
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<BookVo> selectAllBooks(int start, int pageNumber);

	/**     
	 * @Description:TODObookid数
	 * @author: duyi
	 * @date:   2018年11月24日 下午4:14:26    
	 * @return      
	 */  
	int selectBookCounts();

	

	/**     
	 * @Description:TODO根据书名或类别名模糊查询
	 * @author: duyi
	 * @date:   2018年11月25日 上午12:32:10    
	 * @param bookname
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<BookVo> selectBooksByName(@Param(value="bookname")String bookname, int start, int pageNumber);

	/**     
	 * @Description:TODO查询符合条件的书的数量
	 * @author: duyi
	 * @date:   2018年11月25日 下午2:36:16    
	 * @param bookname
	 * @return      
	 */  
	int countBookNum(@Param(value="bookname")String bookname);

	/**     
	 * @Description:TODO通过typename查询booktype信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:04:56    
	 * @param typename
	 * @return      
	 */  
	BookType selectBookTypeByName(@Param("typename")String typename);

	/**     
	 * @Description:TODO添加新书
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:13:44    
	 * @param bv      
	 */  
	void addNewBook(BookVo bv);

	/**     
	 * @Description:TODO变更书的上架下架状态
	 * @author: duyi
	 * @param bookid 
	 * @param bookstate 
	 * @date:   2018年11月25日 下午6:30:03          
	 */  
	void updateBookState(@Param("bookid")int bookid,@Param("bookstate")int bookstate);

	/**     
	 * @Description:TODO根据bookid查询book详细信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:25:27    
	 * @param bookid
	 * @return      
	 */  
	BookVo selectBookInfoById(@Param("bookid")int bookid);

	/**     
	 * @Description:TODO修改书籍信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:58:58    
	 * @param book      
	 */  
	void updateBook(Book book);

	/**
	 * @Description:TODO 书籍剩余数量-1
	 * @author: DongLiang
	 * @date:   2018年11月23日 下午3:44:44    
	 * @param bookid
	 * @return      
	 */  
	int bookSurplusMinusOne(int bookid);

	/**     
	 * @Description:TODO 点击量+1
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午7:30:03    
	 * @return      
	 */  
	int updateClickNum(int bookid);

	/**     
	 * @Description:TODO 书籍剩余量+1
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午1:01:53    
	 * @param bookid      
	 */  
	void bookSurplusAddOne(int bookid);

}
