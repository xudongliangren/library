package com.linkstec.librarysystem.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.vo.UserBorrowVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午8:47:16     
 */
public interface UserBorrowMapper {

	/**     
	 * @Description:TODO 借书 插入一条借书记录
	 * @author: DongLiang
	 * @date:   2018年11月23日 下午3:49:38    
	 * @param bookid
	 * @param userid
	 * @return      
	 */  
	int borrowBook(UserBorrow userborrow);

	/**     
	 * @Description:TODO 查询用户借书记录
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午3:23:03    
	 * @param userid
	 * @return      
	 */  
	List<UserBorrowVo> selectBorrowByUserId(int userid);

	/**     
	 * @Description:TODO  通过借书记录id查询借书记录
	 * @author: DongLiang
	 * @date:   2018年11月27日 上午11:22:37    
	 * @param borrowid
	 * @return      
	 */  
	UserBorrow selectBorrowByBorrowId(int borrowid);

	/**     
	 * @Description:TODO 将借书记录的状态改为已还书，并添加还书时间
	 * @author: DongLiang
	 * @date:   2018年11月27日 上午11:41:31    
	 * @param date
	 * @return      
	 */  
	int returnBook(@Param("borrowid")int borrowid,@Param("date")Date date);

}
