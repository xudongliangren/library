package com.linkstec.librarysystem.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午12:25:30     
 */
public class ToolUtils {
	
	/**     
	 * @Description:TODO 生产随机9位字符串 包含大小写字母或数字
	 * @author: DongLiang
	 * @date:   2018年11月21日 下午12:26:58    
	 * @return      
	 */  
	public static String getString(){
		String az = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		char[] s;
		int ran ;
		String str = "";
		for(int i=0;i<9;i++){
			s = az.toCharArray();
			ran = (int)(Math.random()*(s.length));
			str+=s[ran];
			az.substring(ran,ran+1);
		}
		return str;
	}
	
	/**     
	 * @Description:TODO 获取当前时间
	 * @author: DongLiang
	 * @date:   2018年11月12日 上午9:54:30    
	 * @return      
	 */  
	public static String getCurrentTime(){
		Date date = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sf.format(date);
	}
	
	/**     
	 * @Description:TODO 判断令牌是否可用  true可用，false不可用
	 * @author: DongLiang
	 * @date:   2018年11月12日 上午10:36:58    
	 * @param token
	 * @param req
	 * @return      
	 */  
	public static boolean isRepeatSubmit(double token,HttpServletRequest req){
		double token2 = 0.0;
		token2 = (double)req.getSession().getAttribute("token");
		if(token==token2){
			return true;
		}else{
			return false;
		}
	}
	
	
	/**     
	 * @Description:TODO 制作令牌,返回一个0-1内的7位小数
	 * @author: DongLiang
	 * @date:   2018年11月12日 上午10:16:53    
	 * @return      
	 */  
	public static double makeToken(){
		double token = Math.random();
		int tokenInt = (int)(token*10000000);
		token = (double)tokenInt/10000000;
		return token;
	}
	
}
