package com.linkstec.librarysystem.pojo;
/**     
 * @Description:TODO 书籍类别
 * @author: DongLiang
 * @date:   2018年11月21日 下午1:25:46     
 */
public class BookType {
	
	private int typeid;//书籍类型id
	
	private String typename;//书籍类型名称
	
	private double typerent;//类型书籍的租金
	
	private double breakrent;//逾期租金

	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public double getTyperent() {
		return typerent;
	}

	public void setTyperent(double typerent) {
		this.typerent = typerent;
	}

	public double getBreakrent() {
		return breakrent;
	}

	public void setBreakrent(double breakrent) {
		this.breakrent = breakrent;
	}

	@Override
	public String toString() {
		return "BookType [typeid=" + typeid + ", typename=" + typename + ", typerent=" + typerent + ", breakrent="
				+ breakrent + "]";
	}

	
}
