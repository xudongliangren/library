package com.linkstec.librarysystem.pojo;
/**     
 * @Description:TODO 书籍信息实体类
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:03:59     
 */
public class Book {
	
	private int bookid;//书籍编号
	
	private int typeid;//书籍类型编号
	
	private String bookname;//书籍名称
	
	private int locationid;//书摆放的位置id
	
	private int booknum;//书籍数量
	
	private int booksurplus;//剩余书籍数量
	
	private String summary;//书籍简介
	
	private int clicknum;//点击量
	
	private int bookstate;//书籍是否被禁

	private String resource;//图片资源
	
	private String remark;//备注
	


	public int getBookid() {
		return bookid;
	}

	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	
	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	public int getLocationid() {
		return locationid;
	}

	public void setLocationid(int locationid) {
		this.locationid = locationid;
	}

	public int getBooknum() {
		return booknum;
	}

	public void setBooknum(int booknum) {
		this.booknum = booknum;
	}

	public int getBooksurplus() {
		return booksurplus;
	}

	public void setBooksurplus(int booksurplus) {
		this.booksurplus = booksurplus;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getClicknum() {
		return clicknum;
	}

	public void setClicknum(int clicknum) {
		this.clicknum = clicknum;
	}

	public int getBookstate() {
		return bookstate;
	}

	public void setBookstate(int bookstate) {
		this.bookstate = bookstate;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Book [bookid=" + bookid + ", typeid=" + typeid + ", bookname=" + bookname + ", locationid=" + locationid
				+ ", booknum=" + booknum + ", booksurplus=" + booksurplus + ", summary=" + summary + ", clicknum="
				+ clicknum + ", bookatate=" + bookstate + ", resource=" + resource + ", remark=" + remark + "]";
	}

	
	
}
