package com.linkstec.librarysystem.pojo;

import java.util.Date;

/**     
 * @Description:TODO 借书记录
 * @author: DongLiang
 * @date:   2018年11月21日 下午1:37:42     
 */
public class UserBorrow {
	private int borrowid;//借书记录id
	
	private int userid;//用户id
	
	private int typeid;//类型编号
	
	private int bookid;//书籍编号
	
	private Date borrowtime;//借书日期
	
	private int daylong;//借书时长
	

	private Date returntime;//还书日期


	
	private int state;//借书状态，是否归还

	public int getBorrowid() {
		return borrowid;
	}

	public void setBorrowid(int borrowid) {
		this.borrowid = borrowid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public int getBookid() {
		return bookid;
	}

	public void setBookid(int bookid) {
		this.bookid = bookid;
	}

	public Date getBorrowtime() {
		return borrowtime;
	}

	public void setBorrowtime(Date borrowtime) {
		this.borrowtime = borrowtime;
	}

	public int getDaylong() {
		return daylong;
	}

	public void setDaylong(int daylong) {
		this.daylong = daylong;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	
	public Date getReturntime() {
		return returntime;
	}

	public void setReturntime(Date returntime) {
		this.returntime = returntime;
	}

	@Override
	public String toString() {

		return "UserBorrow [borrowid=" + borrowid + ", userid=" + userid
				+ ", typeid=" + typeid + ", bookid=" + bookid + ", borrowtime="
				+ borrowtime + ", daylong=" + daylong + ", returntime="
				+ returntime + ", state=" + state + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";

	}

	
}
