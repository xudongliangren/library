package com.linkstec.librarysystem.pojo;
/**     
 * @Description:TODO 用户实体类
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:10:08     
 */
public class User {
	private int userid;//用户编号
	
	private String username;//用户id
	
	private String pwd;//用户密码
	
	private double wallet;//账户余额
	
	private String phone;//手机号码
	
	private String idcard;//
	
	private int userstate;//用户状态，禁用或启用
	
	private String remark;//备注

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public double getWallet() {
		return wallet;
	}

	public void setWallet(double wallet) {
		this.wallet = wallet;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public int getUserstate() {
		return userstate;
	}

	public void setUserstate(int userstate) {
		this.userstate = userstate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "User [userid=" + userid + ", username=" + username + ", pwd=" + pwd + ", wallet=" + wallet + ", phone="
				+ phone + ", idcard=" + idcard + ", userstate=" + userstate + ", remark=" + remark + "]";
	}
	
	
}
