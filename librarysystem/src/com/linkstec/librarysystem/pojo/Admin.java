package com.linkstec.librarysystem.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月13日 下午7:51:25     
 */
@Component
public class Admin {
	
	@Value("${adminname}")
	public String adminname;
	
	@Value("${adminpassword}")
	private String adminpassword;

	public String getAdminname() {
		return adminname;
	}

	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}

	public String getAdminpassword() {
		return adminpassword;
	}

	public void setAdminpassword(String adminpassword) {
		this.adminpassword = adminpassword;
	}

	@Override
	public String toString() {
		return "Admin [adminname=" + adminname + ", adminpassword=" + adminpassword + "]";
	}
	
	
}
