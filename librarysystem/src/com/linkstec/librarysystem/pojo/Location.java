package com.linkstec.librarysystem.pojo;
/**     
 * @Description:TODO 书籍位置
 * @author: DongLiang
 * @date:   2018年11月21日 下午1:29:07     
 */
public class Location {
	
	private int locationid;//位置编号
	
	private String locationname;//位置信息

	public int getLocationid() {
		return locationid;
	}

	public void setLocationid(int locationid) {
		this.locationid = locationid;
	}

	public String getLocationname() {
		return locationname;
	}

	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}

	@Override
	public String toString() {
		return "Location [locationid=" + locationid + ", locationname=" + locationname + "]";
	}
	
	
}
