package com.linkstec.librarysystem.pojo;
/**     
 * @Description:TODO 评论实体类
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:32:04     
 */
public class Comment {
	
	private int commentid;//评论id
	
	private int bookid;//书籍id
	
	private int userid;//用户id
	
	private String content;//评论内容

	public int getCommentid() {
		return commentid;
	}

	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}

	public int getBookid() {
		return bookid;
	}

	public void setBookid(int bookid) {
		this.bookid = bookid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Comment [commentid=" + commentid + ", bookid=" + bookid + ", userid=" + userid  + ", content=" + content + "]";
	}

	
}