package com.linkstec.librarysystem.vo;

import com.linkstec.librarysystem.pojo.Comment;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:49:58     
 */
public class CommentVo extends Comment {
	
	private String username;
	
	private String bookname;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	@Override
	public String toString() {
		return "CommentVo [username=" + username + ", bookname=" + bookname
				+ ", getCommentid()=" + getCommentid() + ", getBookid()="
				+ getBookid() + ", getUserid()=" + getUserid()
				+ ", getContent()=" + getContent() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

	
}
