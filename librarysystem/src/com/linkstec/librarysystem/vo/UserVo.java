package com.linkstec.librarysystem.vo;

import com.linkstec.librarysystem.pojo.User;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:50:32     
 */
public class UserVo extends User {
	
	private int borrowNum;

	public int getBorrowNum() {
		return borrowNum;
	}

	public void setBorrowNum(int borrowNum) {
		this.borrowNum = borrowNum;
	}

	@Override
	public String toString() {
		return "UserVo [borrowNum=" + borrowNum + ", getUserid()=" + getUserid() + ", getUsername()=" + getUsername()
				+ ", getPwd()=" + getPwd() + ", getWallet()=" + getWallet() + ", getPhone()=" + getPhone()
				+ ", getIdcard()=" + getIdcard() + ", getUserstate()=" + getUserstate() + ", getRemark()=" + getRemark()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
	
	
}
