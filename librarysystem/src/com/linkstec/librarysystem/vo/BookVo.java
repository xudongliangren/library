package com.linkstec.librarysystem.vo;

import com.linkstec.librarysystem.pojo.Book;



/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:48:51     
 */
public class BookVo extends Book{
	private String typename;
	
	private String locationname;

	private int commentNum;

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public String getLocationname() {
		return locationname;
	}

	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}

	public int getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(int commentNum) {
		this.commentNum = commentNum;
	}

	@Override
	public String toString() {
		return "BookVo [typename=" + typename + ", locationname=" + locationname + ", commentNum=" + commentNum
				+ ", getBookid()=" + getBookid() + ", getTypeid()=" + getTypeid() + ", getBookname()=" + getBookname()
				+ ", getLocationid()=" + getLocationid() + ", getBooknum()=" + getBooknum() + ", getBooksurplus()="
				+ getBooksurplus() + ", getSummary()=" + getSummary() + ", getClicknum()=" + getClicknum()
				+ ", getBookstate()=" + getBookstate() + ", getResource()=" + getResource() + ", getRemark()="
				+ getRemark() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

	
}
