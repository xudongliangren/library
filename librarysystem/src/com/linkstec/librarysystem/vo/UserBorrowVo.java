package com.linkstec.librarysystem.vo;

import com.linkstec.librarysystem.pojo.UserBorrow;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:51:00     
 */
public class UserBorrowVo extends UserBorrow {
	
	private String bookname;
	
	private String typename;

	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	@Override
	public String toString() {
		return "UserBorrowVo [bookname=" + bookname + ", getBorrowid()=" + getBorrowid() + ", getUserid()="
				+ getUserid() + ", getTypeid()=" + getTypeid() + ", getBookid()=" + getBookid() + ", getBorrowtime()="
				+ getBorrowtime() + ", getDaylong()=" + getDaylong() + ", getReturntime()=" + getReturntime()
				+ ", getState()=" + getState() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	
	
}
