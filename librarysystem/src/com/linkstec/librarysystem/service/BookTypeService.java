package com.linkstec.librarysystem.service;

import java.util.List;

import com.linkstec.librarysystem.pojo.BookType;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:54:06     
 */
public interface BookTypeService {

	/**     
	 * @Description:TODO　查询所有booktype信息
	 * @author: duyi
	 * @date:   2018年11月26日 下午2:14:33    
	 * @return      
	 */  
	List<BookType> selectAllBookType();
	
	/**     
	 * @Description:TODO　根据typeid查询booktype信息
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午1:10:47    
	 * @param typeid
	 * @return      
	 */  
	BookType selectTypeById(int typeid);

}
