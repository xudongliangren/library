package com.linkstec.librarysystem.service;

import com.linkstec.librarysystem.pojo.User;

public interface LoginService {

	/**     
	 * @Description:TODO验证用户名和密码
	 * @author: duyi
	 * @date:   2018年11月22日 下午2:23:13    
	 * @param user
	 * @return      
	 */  
	User sbLogin(User user);

}
