package com.linkstec.librarysystem.service;

import java.util.List;

import com.linkstec.librarysystem.pojo.Location;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:55:01     
 */
public interface LocationService {

	/**     
	 * @Description:TODO查询所有location信息
	 * @author: duyi
	 * @date:   2018年11月26日 下午2:18:39    
	 * @return      
	 */  
	List<Location> selectAllLocation();

}
