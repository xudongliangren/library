package com.linkstec.librarysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.mapper.CommentMapper;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.service.CommentService;
import com.linkstec.librarysystem.vo.CommentVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午6:18:06     
 */
@Service
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	private CommentMapper commentMapper;

	@Override//根据书籍id查询所有评论
	public List<CommentVo> selectAllComment(int bookid) {
		return commentMapper.selectAllComment(bookid);
	}

	@Override//添加评论
	public int addComment(Comment comment) {
		return commentMapper.addComment(comment);
	}

	@Override//查询书籍的评论数
	public int selectNumByBookId(int bookid) {
		return commentMapper.selectNumByBookId(bookid);
	}
	
	@Override
	public void deleteComment(int[] commentids) {
		commentMapper.deleteComment(commentids);
	}

	@Override
	public List<CommentVo> checkUserComment(int userid) {
		return commentMapper.checkUserComment(userid);
	}
	
}
