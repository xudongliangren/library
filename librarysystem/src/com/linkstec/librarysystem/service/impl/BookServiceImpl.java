package com.linkstec.librarysystem.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.linkstec.librarysystem.mapper.BookMapper;
import com.linkstec.librarysystem.mapper.CommentMapper;
import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.service.BookService;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.service.UserBorrowService;
import com.linkstec.librarysystem.util.ToolUtils;
import com.linkstec.librarysystem.vo.BookVo;


/**     
 * @Description:TODO 
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:59:38     
 */
@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookMapper bookMapper;
	@Autowired
	private CommentMapper commentMapper;
	
	@Autowired
	private UserBorrowService userBorrowService;

	@Override//通过书籍类别或书籍名称查询书籍 通过点击量排序
	public List<BookVo> selectBookByTypeOrName(Book book, int start, int pageNumber) {
		return bookMapper.selectBookByTypeOrName(book.getTypeid(),book.getBookname(),start,pageNumber);
	}

	@Override//查询符合条件的书籍数量
	public int selectBookNumByTypeOrName(Book book) {
		int a = bookMapper.selectBookNum_ByTypeOrName(book);
		return a;
	}

	@Override//通过书籍id查询书籍信息
	public BookVo selectBookVoById(int bookid) {
		return bookMapper.selectBookVoById(bookid);
	}

	@Override//借书
	@Transactional
	public int borrowBook(UserBorrow userborrow) {
		int flag = 1;
		//书籍剩余数量-1
		int res1 = bookMapper.bookSurplusMinusOne(userborrow.getBookid());
		//根据书籍id查询书籍的类型
		int typeid = bookMapper.selectBookVoById(userborrow.getBookid()).getTypeid();
		
		userborrow.setBorrowtime(new Date());//借书起始时间
		userborrow.setTypeid(typeid);
		userborrow.setState(0);
		//插入一条借书记录
		int res2 = userBorrowService.borrowBook(userborrow);
		return flag;
	}

	@Override//
	public int updateClickNum(int bookid) {
		return bookMapper.updateClickNum(bookid);
	}
	
	@Override
	public List<BookVo> selectAllBooks(int start, int pageNumber) {
		// TODO Auto-generated method stub
		return bookMapper.selectAllBooks(start,pageNumber);
	}

	@Override
	public int selectBookCounts() {
		// TODO Auto-generated method stub
		return bookMapper.selectBookCounts();
	}

	@Override
	public List<BookVo> selectBooksByName(String bookname, int start,
			int pageNumber) {
		// TODO Auto-generated method stub
		return bookMapper.selectBooksByName(bookname,start,pageNumber);
	}

	@Override
	public int countBookNum(String bookname) {
		// TODO Auto-generated method stub
		return bookMapper.countBookNum(bookname);
	}

	@Override
	public BookType selectBookTypeByName(String typename) {
		// TODO Auto-generated method stub
		return bookMapper.selectBookTypeByName(typename);
	}

	@Override
	public void addNewBook(BookVo bv) {
		// TODO Auto-generated method stub
		bookMapper.addNewBook(bv);
	}

	@Override
	public void updateBookState(int bookid,int bookstate) {
		// TODO Auto-generated method stub
		bookMapper.updateBookState(bookid,bookstate);
	}

	@Override
	public BookVo selectBookInfoById(int bookid) {
		// TODO Auto-generated method stub
		return bookMapper.selectBookInfoById(bookid);
	}

	@Override
	public void updateBook(Book book) {
		// TODO Auto-generated method stub
		bookMapper.updateBook(book);
	}

	@Override//书籍的剩余量+1
	public void bookSurplusAddOne(int bookid) {
		bookMapper.bookSurplusAddOne(bookid);
	}

}