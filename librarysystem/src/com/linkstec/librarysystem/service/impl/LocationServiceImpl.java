package com.linkstec.librarysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.mapper.LocationMapper;
import com.linkstec.librarysystem.pojo.Location;
import com.linkstec.librarysystem.service.LocationService;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午6:18:36     
 */
@Service
public class LocationServiceImpl implements LocationService {
	
	@Autowired
	private LocationMapper locationMapper;

	@Override
	public List<Location> selectAllLocation() {
		// TODO Auto-generated method stub
		return locationMapper.selectAllLocation();
	}
}
