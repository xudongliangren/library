package com.linkstec.librarysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.mapper.BookTypeMapper;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.service.BookTypeService;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午6:10:59     
 */
@Service
public class BookTypeImpl implements BookTypeService {
	
	@Autowired
	private BookTypeMapper bookTypeMapper;

	@Override
	public List<BookType> selectAllBookType() {
		// TODO Auto-generated method stub
		return bookTypeMapper.selectAllBookType();
	}

	@Override//根据typeid查询booktype信息上
	public BookType selectTypeById(int typeid) {
		return bookTypeMapper.selectTypeById(typeid);
	}
}
