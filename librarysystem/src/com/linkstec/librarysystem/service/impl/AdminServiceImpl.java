package com.linkstec.librarysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.pojo.Location;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.service.AdminService;
import com.linkstec.librarysystem.service.BookService;
import com.linkstec.librarysystem.service.BookTypeService;
import com.linkstec.librarysystem.service.CommentService;
import com.linkstec.librarysystem.service.LocationService;
import com.linkstec.librarysystem.service.UserService;
import com.linkstec.librarysystem.util.Page;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.vo.UserBorrowVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月23日 上午9:21:49     
 */
@Service
public class AdminServiceImpl implements AdminService {
	@Autowired
	private UserService userService;
	@Autowired
	private BookService bookService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private BookTypeService btService;
	@Autowired
	private LocationService locationService;

	@Override
	public Page showAllUsers(Page page) {
		// TODO Auto-generated method stub
		return userService.showAllUsers(page);
	}

	@Override
	public Page selectUsersByName(String username, Page page) {
		// TODO Auto-generated method stub
		return userService.selectUsersByName(username,page);
	}

	@Override
	public List<UserBorrowVo> showUserLog(int userid) {
		// TODO Auto-generated method stub
		return userService.showUserLog(userid);
	}

	@Override
	public User toUpdateUser(int userid) {
		// TODO Auto-generated method stub
		return userService.selectUserById(userid);
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		userService.updateUser(user);
	}

	@Override
	public Page showAllBooks(Page page) {
		// TODO Auto-generated method stub
		int start = (page.getCurPage()-1)*page.getPageNumber(); 
		List<BookVo>booklist = bookService.selectAllBooks(start,page.getPageNumber());
		int rows = bookService.selectBookCounts();
		int totalPage = rows % page.getPageNumber()==0?rows/page.getPageNumber():rows/page.getPageNumber()+1;
		page.setDate(booklist);
		page.setRows(rows);
		page.setTotalPage(totalPage);
		return page;
	}

	@Override
	public Page selectBooksByName(String bookname, Page page) {
		// TODO Auto-generated method stub
		int start = (page.getCurPage()-1)*page.getPageNumber(); 
		List<BookVo>list = bookService.selectBooksByName(bookname,start,page.getPageNumber());
		int rows = bookService.countBookNum(bookname);
		int totalPage = rows % page.getPageNumber()==0?rows/page.getPageNumber():rows/page.getPageNumber()+1;
		page.setDate(list);
		page.setRows(rows);
		page.setTotalPage(totalPage);
		return page;
	}

	@Override
	public BookType selectBookTypeByName(String typename) {
		// TODO Auto-generated method stub
		return bookService.selectBookTypeByName(typename);
	}

	@Override
	public void addNewBook(BookVo bv) {
		// TODO Auto-generated method stub
		bookService.addNewBook(bv);
	}

	@Override
	public void updateBookState(int bookid,int bookstate) {
		// TODO Auto-generated method stub
		bookService.updateBookState(bookid,bookstate);
	}

	@Override
	public BookVo toUpdateBook(int bookid) {
		// TODO Auto-generated method stub
		return bookService.selectBookInfoById(bookid);
	}

	@Override
	public void updateBook(Book book) {
		// TODO Auto-generated method stub
		bookService.updateBook(book);
	}

	@Override
	public List<CommentVo> checkBookComment(int bookid) {
		// TODO Auto-generated method stub
		return commentService.selectAllComment(bookid);
	}

	@Override
	public void deleteComment(int[] commentids) {
		// TODO Auto-generated method stub
		commentService.deleteComment(commentids);
	}

	@Override
	public List<CommentVo> checkUserComment(int userid) {
		// TODO Auto-generated method stub
		return commentService.checkUserComment(userid);
	}

	@Override
	public List<BookType> selectAllBookType() {
		// TODO Auto-generated method stub
		return btService.selectAllBookType();
	}

	@Override
	public List<Location> selectAllLocation() {
		// TODO Auto-generated method stub
		return locationService.selectAllLocation();
	}

	@Override
	public void changeUserstate(int userid, int userstate) {
		// TODO Auto-generated method stub
		userService.changeUserstate(userid,userstate);
	}
	
}
