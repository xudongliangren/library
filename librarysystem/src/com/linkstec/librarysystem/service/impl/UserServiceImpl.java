package com.linkstec.librarysystem.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.linkstec.librarysystem.mapper.UserMapper;
import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.service.BookService;
import com.linkstec.librarysystem.service.BookTypeService;
import com.linkstec.librarysystem.service.CommentService;
import com.linkstec.librarysystem.service.UserBorrowService;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.service.UserService;
import com.linkstec.librarysystem.util.Page;
import com.linkstec.librarysystem.util.TimeTest;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.vo.UserBorrowVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午6:19:47     
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private CommentService commentSerrvice;
	
	@Autowired
	private BookService bookService;
	
	@Autowired
	private UserBorrowService userBorrowService;
	
	@Autowired
	private BookTypeService bookTypeService;
	

	@Override//查询所有书籍 按热度排序 分页
	public Page selectAllBook(Page page) {
		Book book = new Book();
		BookVo bookVo = null;
		book.setBookname("");
		book.setTypeid(0);
		int start = (page.getCurPage()-1)*page.getPageNumber();//获取该页起始查询点
		//根据通过书籍类别或书籍名称查询书籍
		List<BookVo> bookList = bookService.selectBookByTypeOrName(book,start,page.getPageNumber());
		for(int i = 0; i<bookList.size();i++){
			bookVo = bookList.get(i);
			bookVo.setCommentNum(commentSerrvice.selectNumByBookId(bookVo.getBookid()));
			bookList.set(i, bookVo);
		}
		//查询符合条件的书籍数量
		int rows = bookService.selectBookNumByTypeOrName(book);
		//总页数
		int totalPage = rows % page.getPageNumber()==0?rows/page.getPageNumber():rows/page.getPageNumber()+1;
		page.setRows(rows);
		page.setDate(bookList);
		page.setTotalPage(totalPage);
		return page;
	}
	

	@Override//通过书籍类别或书籍名称查询书籍   通过点击量排序 且分页页
	public Page selectBookByTypeOrName(Page page,Book book) {
		BookVo bookVo = null;
		int start = (page.getCurPage()-1)*page.getPageNumber();//获取该页起始查询点
		//根据通过书籍类别或书籍名称查询书籍
		List<BookVo> bookList = bookService.selectBookByTypeOrName(book,start,page.getPageNumber());
		for(int i = 0; i<bookList.size();i++){
			bookVo = bookList.get(i);
			bookVo.setCommentNum(commentSerrvice.selectNumByBookId(bookVo.getBookid()));
			bookList.set(i, bookVo);
		}
		//查询符合条件的书籍数量
		int rows = bookService.selectBookNumByTypeOrName(book);
		//总页数
		int totalPage = rows % page.getPageNumber()==0?rows/page.getPageNumber():rows/page.getPageNumber()+1;
		page.setRows(rows);
		page.setDate(bookList);
		page.setTotalPage(totalPage);
		return page;
	}

	@Override//通过书籍id查询书籍信息
	public BookVo selectBookVoById(int bookid) {
		bookService.updateClickNum(bookid);//点击量+1
		return bookService.selectBookVoById(bookid);
	}

	@Override//根据书籍id查询所有评论
	public List<CommentVo> selectAllComment(int bookid) {
		return commentSerrvice.selectAllComment(bookid);
	}

	@Override//添加评论
	public int addComment(Comment comment) {
		return commentSerrvice.addComment(comment);
	}

	@Override
	public void registerUser(User user) {
		userMapper.registerUser(user);
	}

	@Override
	public int checkUser(User user) {
		return userMapper.checkUser(user);
	}

	@Override
	public User sbLogin(User user) {
		return userMapper.sbLogin(user);
	}

	@Override//用户借书
	public int borrowBook(UserBorrow userborrow) {
		int flag = bookService.borrowBook(userborrow);
		return flag;
	}

	@Override
	public Page showAllUsers(Page page) {//分页查询所有用户
		int start =  (page.getCurPage()-1)*page.getPageNumber();
		List<User>list = userMapper.showAllUsers(start,page.getPageNumber());
		int rows = userMapper.selectUserCount();
		int totalPage = rows % page.getPageNumber()==0?rows/page.getPageNumber():rows/page.getPageNumber()+1;
		page.setRows(rows);
		page.setDate(list);
		page.setTotalPage(totalPage);
		return page;
	}

	@Override
	public Page selectUsersByName(String username, Page page) {//根据用户名分页模糊查询用户
		int start =  (page.getCurPage()-1)*page.getPageNumber();
		List<User> list = userMapper.selectUsersByName(username,start,page.getPageNumber());
		int rows = userMapper.selectUserCountByName(username);
		int totalPage = rows % page.getPageNumber()==0?rows/page.getPageNumber():rows/page.getPageNumber()+1;
		page.setRows(rows);
		page.setDate(list);
		page.setTotalPage(totalPage);
		return page;
	}

	@Override
	public List<UserBorrowVo> showUserLog(int userid) {
		return userMapper.showUserLog(userid);
	}

	@Override
	public User selectUserById(int userid) {
		return userMapper.selectUserById(userid);
	}

	@Override
	public void updateUser(User user) {
		userMapper.updateUser(user);
	}

	@Override
	public List<UserBorrowVo> selectBorrowByUserId(int userid) {//查询用户记录
		return userBorrowService.selectBorrowByUserId(userid);
	}
	
	@Override
	public void changeUserstate(int userid, int userstate) {
		userMapper.changeUserstate(userid,userstate);
	}


	@Override//还书结算
	@Transactional
	public double returnBook(int borrowid) {
		//将借书记录的状态改为已还书，并添加还书时间 书籍剩余量+1
		Date date = new Date();
		int flag = userBorrowService.returnBook(borrowid,date);
		UserBorrow userBorrow = userBorrowService.selectBorrowByBorrowId(borrowid);
		//该书籍剩余量+1
		bookService.bookSurplusAddOne(userBorrow.getBookid());
		//查询书籍类型的收费标准
		BookType type = bookTypeService.selectTypeById(userBorrow.getTypeid());
		//计算借书天数
		int day = TimeTest.getDiscrepantDays(userBorrow.getBorrowtime(), userBorrow.getReturntime());
		//计算租金
		double money = 0;
		if(day<userBorrow.getDaylong()&&day<5){
			money = userBorrow.getDaylong()*type.getTyperent();
		}else{
			money = 5*type.getTyperent()+(day-userBorrow.getDaylong())*type.getBreakrent();
		}
		//扣钱
		userMapper.pay(money,userBorrow.getUserid());
		return money;
	}


	@Override
	public int addWallet(User user) {		
		return userMapper.addWallet(user);
	}


	@Override
	public void changeUser(User user1) {
		userMapper.changeUser(user1);
		
	}

}
