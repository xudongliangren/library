package com.linkstec.librarysystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.service.RegisterService;
import com.linkstec.librarysystem.service.UserService;
@Service
public class RegisterServiceImpl implements RegisterService{
	@Autowired
	private UserService userService;
	
	@Override
	public void registerUser(User user) {
		// TODO Auto-generated method stub
		userService.registerUser(user);
	}

	@Override
	public int checkUser(User user) {
		// TODO Auto-generated method stub
		return userService.checkUser(user);
	}

}
