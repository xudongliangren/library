package com.linkstec.librarysystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.service.LoginService;
import com.linkstec.librarysystem.service.UserService;
@Service
public class LoginServiceImpl implements LoginService{
	@Autowired
	private UserService userService;

	@Override
	public User sbLogin(User user) {
		// TODO Auto-generated method stub
		return userService.sbLogin(user);
	}

}
