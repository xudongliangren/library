package com.linkstec.librarysystem.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstec.librarysystem.service.UserBorrowService;
import com.linkstec.librarysystem.vo.UserBorrowVo;
import com.linkstec.librarysystem.mapper.UserBorrowMapper;
import com.linkstec.librarysystem.pojo.UserBorrow;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午6:19:24     
 */
@Service
public class UserBorrowImpl implements UserBorrowService {
	
	@Autowired
	private UserBorrowMapper userBorrowMapper;

	@Override
	public int borrowBook(UserBorrow userborrow) {
		return userBorrowMapper.borrowBook(userborrow);
	}

	@Override
	public List<UserBorrowVo> selectBorrowByUserId(int userid) {//查询用户借书记录
		return userBorrowMapper.selectBorrowByUserId(userid);
	}

	@Override//通过借书记录id查询借书记录
	public UserBorrow selectBorrowByBorrowId(int borrowid) {
		return userBorrowMapper.selectBorrowByBorrowId(borrowid);
	}

	@Override
	public int returnBook(int borrowid,Date date) {//将借书记录的状态改为已还书，并添加还书时间
		return userBorrowMapper.returnBook(borrowid,date);
	}
}
