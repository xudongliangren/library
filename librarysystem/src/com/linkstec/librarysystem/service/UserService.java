package com.linkstec.librarysystem.service;

import java.util.List;

import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.util.Page;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.vo.UserBorrowVo;
import com.linkstec.librarysystem.vo.UserVo;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.pojo.UserBorrow;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:55:20     
 */
public interface UserService {
	

	/**     
	 * @Description:TODO 查询所有书籍 按热度排序 分页
	 * @author: DongLiang
	 * @date:   2018年11月24日 下午3:55:37    
	 * @param page
	 * @return      
	 */  
	Page selectAllBook(Page page);

	/**     
	 * @Description:TODO 通过书籍类别或书籍名称查询书籍 通过点击量排序
	 * @author: DongLiang
	 * @date:   2018年11月22日 上午10:47:05    
	 * @param book
	 * @return      
	 */  
	Page selectBookByTypeOrName(Page page,Book book);

	/**     
	 * @Description:TODO 通过书籍id查询书籍信息
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:10:04    
	 * @param bookid
	 * @return      
	 */  
	BookVo selectBookVoById(int bookid);

	/**     
	 * @Description:TODO 查询所有评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:28:43    
	 * @param bookid
	 * @return      
	 */  
	List<CommentVo> selectAllComment(int bookid);

	/**     
	 * @Description:TODO 添加评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:53:25    
	 * @param comment      
	 */  
	int addComment(Comment comment);

	/**
	 * @Description:TODO注册新用户
	 * @author: duyi
	 * @date:   2018年11月22日 上午10:55:18    
	 * @param user
	 * @return      
	 */  
	void registerUser(User user);

	/**     
	 * @Description:TODO检查用户名是否存在
	 * @author: duyi
	 * @date:   2018年11月22日 上午11:00:03    
	 * @param user
	 * @return      
	 */  
	int checkUser(User user);

	/**     
	 * @Description:TODO验证用户名和密码
	 * @author: duyi
	 * @date:   2018年11月22日 下午2:24:49    
	 * @param user
	 * @return      
	 */  
	User sbLogin(User user);

	/**     
	 * @Description:TODO
	 * @author: DongLiang
	 * @date:   2018年11月23日 下午4:07:54    
	 * @param userborrow
	 * @return      
	 */  
	int borrowBook(UserBorrow userborrow);

	/**
	 * @Description:TODO分页查询所有用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:26:02    
	 * @param page
	 * @return      
	 */  
	Page showAllUsers(Page page);

	/**     
	 * @Description:TODO根据用户名分页模糊查询用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:53:02    
	 * @param username
	 * @param page
	 * @return      
	 */  
	Page selectUsersByName(String username, Page page);

	/**     
	 * @Description:TODO 查询用户借书记录
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午3:17:50    
	 * @param userid
	 * @return      
	 */  
	List<UserBorrowVo> selectBorrowByUserId(int userid);

	/**
	 * @Description:TODO查看用户借书历史
	 * @author: duyi
	 * @date:   2018年11月23日 下午3:25:09    
	 * @param userid
	 * @return      
	 */  
	List<UserBorrowVo> showUserLog(int userid);

	/**     
	 * @Description:TODO查询出该用户的信息并跳转到修改用户信息页面
	 * @author: duyi
	 * @date:   2018年11月24日 下午2:13:19    
	 * @param userid
	 * @return      
	 */  
	User selectUserById(int userid);

	/**     
	 * @Description:TODO修改用户信息
	 * @author: duyi
	 * @date:   2018年11月24日 下午3:23:17    
	 * @param user      
	 */  
	void updateUser(User user);

	/**     
	 * @Description:TODO变更用户启用禁用状态
	 * @author: duyi
	 * @date:   2018年11月26日 下午9:34:16    
	 * @param userid
	 * @param userstate      
	 */  
	void changeUserstate(int userid, int userstate);

	/**     
	 * @Description:TODO 还书
	 * @author: DongLiang
	 * @date:   2018年11月27日 上午11:12:32    
	 * @param borrowid
	 * @return      
	 */  
	double returnBook(int borrowid);

	/**     
	 * @Description:TODO  充值
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午4:16:09    
	 * @param userid
	 * @return      
	 */  
	int addWallet(User user);

	/**     
	 * @Description:TODO   修改个人信息
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午5:27:18    
	 * @param user1      
	 */  
	void changeUser(User user1);

}
