package com.linkstec.librarysystem.service;

import java.util.List;

import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.vo.CommentVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:54:39     
 */
public interface CommentService {

	/**     
	 * @Description:TODO 根据书籍id查询所有评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:30:50    
	 * @param bookid
	 * @return      
	 */  
	List<CommentVo> selectAllComment(int bookid);

	/**     
	 * @Description:TODO 添加评论
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:57:17    
	 * @param comment
	 * @return      
	 */  
	int addComment(Comment comment);
	
	/**     
	 * @Description:TODO 查询书籍的评论数
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午5:17:30    
	 * @param bookid
	 * @return      
	 */  
	int selectNumByBookId(int bookid);

	/**     
	 * @Description:TODO根据commentid删除评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:02:31    
	 * @param commentids      
	 */  
	void deleteComment(int[] commentids);

	/**     
	 * @Description:TODO根据userid查看该用户的所有评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:24:49    
	 * @param userid
	 * @return      
	 */  
	List<CommentVo> checkUserComment(int userid);

}
