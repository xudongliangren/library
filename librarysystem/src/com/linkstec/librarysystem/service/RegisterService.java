package com.linkstec.librarysystem.service;

import com.linkstec.librarysystem.pojo.User;

public interface RegisterService {

	/**     
	 * @Description:TODO添加新用户
	 * @author: duyi
	 * @date:   2018年11月22日 上午10:50:12    
	 * @param user
	 * @return      
	 */  
	void registerUser(User user);

	/**     
	 * @Description:TODO检测用户名是否存在
	 * @author: duyi
	 * @date:   2018年11月22日 上午10:58:23    
	 * @param user
	 * @return      
	 */ 
	int checkUser(User user);

}
