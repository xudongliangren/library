package com.linkstec.librarysystem.service;

import java.util.List;

import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.pojo.Comment;
import com.linkstec.librarysystem.pojo.Location;
import com.linkstec.librarysystem.pojo.User;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.util.Page;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.vo.CommentVo;
import com.linkstec.librarysystem.vo.UserBorrowVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月23日 上午9:21:36     
 */
public interface AdminService {

	/**     
	 * @Description:TODO分页查询所有用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:24:40    
	 * @param page
	 * @return      
	 */  
	Page showAllUsers(Page page);

	/**     
	 * @Description:TODO根据用户名分页模糊查询用户
	 * @author: duyi
	 * @date:   2018年11月23日 上午11:51:28    
	 * @param username
	 * @param page      
	 * @return 
	 */  
	Page selectUsersByName(String username, Page page);

	/**     
	 * @Description:TODO查看用户借书历史
	 * @author: duyi
	 * @date:   2018年11月23日 下午3:24:16    
	 * @param userid
	 * @return      
	 */  
	List<UserBorrowVo> showUserLog(int userid);

	/**     
	 * @Description:TODO查询出该用户的信息并跳转到修改用户信息页面
	 * @author: duyi
	 * @date:   2018年11月24日 下午2:09:53    
	 * @param userid
	 * @return      
	 */  
	User toUpdateUser(int userid);

	/**     
	 * @Description:TODO修改用户信息
	 * @author: duyi
	 * @date:   2018年11月24日 下午2:45:06    
	 * @param user      
	 */  
	void updateUser(User user);

	/**     
	 * @Description:TODO分页查询所有图书
	 * @author: duyi
	 * @date:   2018年11月24日 下午4:01:42    
	 * @param page
	 * @return      
	 */  
	Page showAllBooks(Page page);

	/**     
	 * @Description:TODO根据书名或类别名模糊查询
	 * @author: duyi
	 * @date:   2018年11月25日 上午12:23:41    
	 * @param bookname
	 * @param page
	 * @return      
	 */  
	Page selectBooksByName(String bookname, Page page);

	/**     
	 * @Description:TODO通过typename查询booktype信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:02:49    
	 * @param typename
	 * @return      
	 */  
	BookType selectBookTypeByName(String typename);

	/**     
	 * @Description:TODO添加新书
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:12:23    
	 * @param bv      
	 */  
	void addNewBook(BookVo bv);

	/**     
	 * @Description:TODO变更书的上架下架状态
	 * @author: duyi
	 * @param bookid 
	 * @param bookstate 
	 * @date:   2018年11月25日 下午6:28:34          
	 */  
	void updateBookState(int bookid, int bookstate);

	/**     
	 * @Description:TODO跳转到修改页面，并把用户详细信息展示出来
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:21:53    
	 * @param bookid
	 * @return      
	 */  
	BookVo toUpdateBook(int bookid);

	/**     
	 * @Description:TODO修改书籍信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:57:42    
	 * @param book      
	 */  
	void updateBook(Book book);

	/**     
	 * @Description:TODO查看改善书籍的评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午8:36:06    
	 * @param bookid
	 * @return      
	 */  
	List<CommentVo> checkBookComment(int bookid);

	/**     
	 * @Description:TODO根据commentid删除评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午8:59:20    
	 * @param commentids      
	 */  
	void deleteComment(int[] commentids);

	/**     
	 * @Description:TODO根据userid查看该用户的所有评论
	 * @author: duyi
	 * @date:   2018年11月25日 下午9:21:24    
	 * @param userid
	 * @return      
	 */  
	List<CommentVo> checkUserComment(int userid);

	/**     
	 * @Description:TODO查询所有booktype信息
	 * @author: duyi
	 * @date:   2018年11月26日 下午2:12:43    
	 * @return      
	 */  
	List<BookType> selectAllBookType();

	/**     
	 * @Description:TODO查询所有location信息
	 * @author: duyi
	 * @date:   2018年11月26日 下午2:17:47    
	 * @return      
	 */  
	List<Location> selectAllLocation();

	/**     
	 * @Description:TODO变更用户启用禁用状态
	 * @author: duyi
	 * @date:   2018年11月26日 下午9:32:24    
	 * @param userid
	 * @param userstate      
	 */  
	void changeUserstate(int userid, int userstate);

	

}
