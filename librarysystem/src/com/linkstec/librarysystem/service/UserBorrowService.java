package com.linkstec.librarysystem.service;

import java.util.Date;
import java.util.List;

import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.vo.UserBorrowVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月21日 下午4:55:39     
 */
public interface UserBorrowService {

	/**     
	 * @Description:TODO 借书功能   插入借书记录
	 * @author: DongLiang
	 * @date:   2018年11月23日 下午3:47:12    
	 * @param bookid
	 * @param userid
	 * @return      
	 */  
	int borrowBook(UserBorrow userborrow);

	/**     
	 * @Description:TODO 查询用户借书记录
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午3:21:24    
	 * @param userid
	 * @return      
	 */  
	List<UserBorrowVo> selectBorrowByUserId(int userid);

	/**     
	 * @Description:TODO　通过借书记录id查询借书记录
	 * @author: DongLiang
	 * @date:   2018年11月27日 上午11:21:11    
	 * @param borrowid
	 * @return      
	 */  
	UserBorrow selectBorrowByBorrowId(int borrowid);

	/**     
	 * @Description:TODO 将借书记录的状态改为已还书，并添加还书时间
	 * @author: DongLiang
	 * @date:   2018年11月27日 上午11:40:47    
	 * @param date
	 * @return      
	 */  
	int returnBook(int borrowid,Date date);

}
