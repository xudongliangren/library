package com.linkstec.librarysystem.service;

import java.util.List;

import com.linkstec.librarysystem.pojo.Book;
import com.linkstec.librarysystem.pojo.BookType;
import com.linkstec.librarysystem.vo.BookVo;
import com.linkstec.librarysystem.pojo.UserBorrow;
import com.linkstec.librarysystem.vo.BookVo;

/**     
 * @Description:TODO
 * @author: DongLiang
 * @date:   2018年11月20日 下午5:12:15     
 */
public interface BookService {

	/**     
	 * @Description:TODO 根据通过书籍类别或书籍名称查询书籍
	 * @author: DongLiang
	 * @date:   2018年11月22日 上午11:05:43    
	 * @param book
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<BookVo> selectBookByTypeOrName(Book book, int start, int pageNumber);

	/**     
	 * @Description:TODO 查询符合条件的书的数量
	 * @author: DongLiang
	 * @date:   2018年11月22日 上午11:05:45    
	 * @param book
	 * @return      
	 */  
	int selectBookNumByTypeOrName(Book book);

	/**     
	 * @Description:TODO 通过书籍id查询书籍
	 * @author: DongLiang
	 * @date:   2018年11月22日 下午2:11:21    
	 * @param bookid
	 * @return      
	 */  
	BookVo selectBookVoById(int bookid);
	
	/**     
	 * @Description:TODO 点击量+1
	 * @author: DongLiang
	 * @date:   2018年11月26日 下午7:29:17    
	 * @return      
	 */  
	int updateClickNum(int bookid);

	/**     
	 * @Description:TODO 借书
	 * @author: DongLiang
	 * @date:   2018年11月23日 下午2:24:02    
	 * @param userid
	 * @param bookid
	 * @return      
	 */  
	int borrowBook(UserBorrow userborrow);

	/**     
	 * @Description:TODO分页查询所有图书
	 * @author: duyi
	 * @date:   2018年11月24日 下午4:07:16    
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<BookVo> selectAllBooks(int start, int pageNumber);

	/**     
	 * @Description:TODO查询bookid数
	 * @author: duyi
	 * @date:   2018年11月24日 下午4:12:18    
	 * @return      
	 */  
	int selectBookCounts();

	/**     
	 * @Description:TODO根据书名或类别名模糊查询
	 * @author: duyi
	 * @date:   2018年11月25日 上午12:30:21    
	 * @param bookname
	 * @param start
	 * @param pageNumber
	 * @return      
	 */  
	List<BookVo> selectBooksByName(String bookname, int start, int pageNumber);

	/**     
	 * @Description:TODO查询符合条件的书的数量
	 * @author: duyi
	 * @date:   2018年11月25日 下午2:34:47    
	 * @param bookname
	 * @return      
	 */  
	int countBookNum(String bookname);

	/**     
	 * @Description:TODO通过typename查询booktype信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:04:13    
	 * @param typename
	 * @return      
	 */  
	BookType selectBookTypeByName(String typename);

	/**     
	 * @Description:TODO添加新书
	 * @author: duyi
	 * @date:   2018年11月25日 下午5:13:01    
	 * @param bv      
	 */  
	void addNewBook(BookVo bv);

	/**     
	 * @Description:TODO变更书的上架下架状态
	 * @author: duyi
	 * @param bookid 
	 * @param bookstate 
	 * @date:   2018年11月25日 下午6:29:21          
	 */  
	void updateBookState(int bookid, int bookstate);

	/**     
	 * @Description:TODO根据bookid查询book的详细信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:24:41    
	 * @param bookid
	 * @return      
	 */  
	BookVo selectBookInfoById(int bookid);

	/**     
	 * @Description:TODO修改书籍信息
	 * @author: duyi
	 * @date:   2018年11月25日 下午7:58:21    
	 * @param book      
	 */  
	void updateBook(Book book);

	/**     
	 * @Description:TODO 书籍剩余量+1
	 * @author: DongLiang
	 * @date:   2018年11月27日 下午1:00:53    
	 * @param bookid      
	 */  
	void bookSurplusAddOne(int bookid);




}
